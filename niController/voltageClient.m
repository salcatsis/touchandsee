% Endlessly stream voltage data acquired by a NI-6001 device over UDP

% Cleanup from previous run
if exist('daqSession')
    release(daqSession)
end 

if exist('udp')
    fclose(udp)
end

clear all

% Main settings
daqName = 'Dev5';
controlFrequency = 20000;
dispatchRate = 20;
destination_ip = '192.168.0.101';
destination_port = 5500;

% UDP Client
udpClient = udp(destination_ip,destination_port);
fopen(udpClient);

% Setup DAQ
daqSession = daq.createSession('ni');
daqSession.Rate = controlFrequency;
daqSession.NotifyWhenDataAvailableExceeds = controlFrequency / dispatchRate;
daqSession.IsContinuous = true;

% A single differential analog input (+/- 10V)
voltageMonitor = addAnalogInputChannel(daqSession, daqName, 'ai0', 'Voltage');
voltageMonitor.TerminalConfig = 'Differential';

% Acquisition Event
lh = addlistener(daqSession, 'DataAvailable', @(src, event)voltageDispatcher(src, event, udpClient));
daqSession.startBackground();

% LMK when to stop
disp('Acquiring Data')
while ('q' ~= input('Press q to quit: ', 's'))
end
daqSession.stop();




