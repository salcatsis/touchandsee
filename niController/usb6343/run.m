close all
offTime = 2;
onTime = 50;
onVoltage = 3000;
onVoltageSignal = (2/1000)*onVoltage;

outputVoltagesOff = zeros(offTime*controlFrequency,3);
outputVoltagesOn = zeros(onTime*controlFrequency,3);
outputVoltagesOn(1:end,2) = onVoltageSignal*ones(size(outputVoltagesOn,1),1);
%outputVoltagesOn(1:end,3) = onVoltageSignal*ones(size(outputVoltagesOn,1),1);
% outputVoltagesOn2 = zeros(onTime*controlFrequency,3);
% outputVoltagesOn2(1:end,1) = -(onVoltageSignal+1)*ones(size(outputVoltagesOn2,1),1);
% outputVoltagesOn2(1:end,3) = (onVoltageSignal+1)*ones(size(outputVoltagesOn2,1),1);

outputVoltages = [outputVoltagesOff; outputVoltagesOn; outputVoltagesOff];
queueOutputData(highVoltageControlSession, outputVoltages);

[data,time] = highVoltageControlSession.startForeground;

stroke = 10*mean(data(1000*(offTime+onTime-1):1000*(offTime+onTime),1)) - 10*mean(data(1:1000*offTime,1));
 
strokeTime = (find(smooth(data(:,1),10) > (mean(data(1:1000*offTime,1)) + 0.95*stroke/10),1))/1000 - offTime;

plotData;

pause(1)

close(dataFigure);

disp('stroke = ');
disp(stroke);

disp('stroke time = ');
disp(strokeTime);

disp('save that data!');