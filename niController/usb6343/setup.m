controlFrequency = 1000;

highVoltageControlSession = c

Laser = addAnalogInputChannel(highVoltageControlSession,'Dev4','ai0','Voltage');
voltageMonitor1 = addAnalogInputChannel(highVoltageControlSession,'Dev4','ai1','Voltage');
curentMonitor1 = addAnalogInputChannel(highVoltageControlSession,'Dev4','ai2','Voltage');
voltageMonitor2 = addAnalogInputChannel(highVoltageControlSession,'Dev4','ai3','Voltage');
curentMonitor2 = addAnalogInputChannel(highVoltageControlSession,'Dev4','ai4','Voltage');
voltageMonitor3 = addAnalogInputChannel(highVoltageControlSession,'Dev4','ai5','Voltage');
curentMonitor3 = addAnalogInputChannel(highVoltageControlSession,'Dev4','ai6','Voltage');

Laser.TerminalConfig = 'Differential';
voltageMonitor1.TerminalConfig = 'Differential';
curentMonitor1.TerminalConfig = 'Differential';
voltageMonitor2.TerminalConfig = 'Differential';
curentMonitor2.TerminalConfig = 'Differential';
voltageMonitor3.TerminalConfig = 'Differential';
curentMonitor3.TerminalConfig = 'Differential';

voltageProgramming1 = addAnalogOutputChannel(highVoltageControlSession,'Dev4','ao1','Voltage');
voltageProgramming2 = addAnalogOutputChannel(highVoltageControlSession,'Dev4','ao2','Voltage');
voltageProgramming3 = addAnalogOutputChannel(highVoltageControlSession,'Dev4','ao3','Voltage');

highVoltageControlSession.Rate = controlFrequency;

outputSingleScan(highVoltageControlSession,[0 0 0]);