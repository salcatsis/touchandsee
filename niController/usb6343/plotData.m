dataFigure = figure;
subplot(3,1,1), plot(time,(5000/10)*data(:,[2 4 6]));
xlabel('time /s')
ylabel('voltage /V')
legend('channel 1','channel 2','channel 3','Location','NorthOutside');

subplot(3,1,2), plot(time,(200/10)*data(:,[3 5 7]));
xlabel('time /s')
ylabel('current /uA')

subplot(3,1,3), plot(time,10*data(:,1),[time(1) time(end)],10*[(mean(data(1:1000*offTime,1)) + stroke/10) (mean(data(1:1000*offTime,1)) + stroke/10)]);
xlabel('time /s')
ylabel('displacement /mm')