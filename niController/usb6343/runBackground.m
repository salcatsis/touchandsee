close all

onTime = 2;
onVoltage = 3000;
onVoltageSignal = (2/1000)*onVoltage;

% TODO: A data logger
lh = addlistener(highVoltageControlSession,'DataAvailable', @backgroundPlot);

% TODO: Generalise for number of cells
% Turn them all on
onData = [onVoltageSignal, onVoltageSignal, onVoltageSignal];
lh = addlistener(highVoltageControlSession,'DataRequired', ...
        @(src,event) src.queueOutputData(onData));

% Queue initial output data and start
queueOutputData(highVoltageControlSession, onData)
startBackground(highVoltageControlSession)

% BURN IT UP!
% TODO: Something builting sleep function?
disp(strcat('Running at  ', num2str(onVoltage), ' V'))
while input('Type q to quit: ','s') ~= 'q'
end