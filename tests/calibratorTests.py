import unittest
from mock import patch, MagicMock, Mock

from patternGenerator.tracker.calibrator import Calibrator
import patternGenerator.settings as settings

import time
import testUtils

class CalibratorTests(unittest.TestCase):
    def setUp(self):
        # Mocked PointGreyCamera
        self.camera_patcher = patch("patternGenerator.tracker.calibrator.PointGreyCamera")
        camera_mock = self.camera_patcher.start()

        # The mock camera returns a still test image
        camera_mock.return_value.get_frame.return_value = testUtils.test_image()

    @patch('patternGenerator.tracker.calibrator.cv2.imshow')
    def test_calibrator(self, imshow_mock, timeout=0.1):
        calibrator = Calibrator(settings.load()["tracker"])

        # Take a test frame
        time.sleep(timeout)
        
        # Test thread is alive
        self.assertTrue(calibrator.isAlive())

        calibrator.quit()
        self.assertFalse(calibrator.isAlive())



    def tearDown(self):
        self.camera_patcher.stop()
