import unittest
from patternGenerator.experimentation.experiments.patterns.pattern import Pattern

class PatternTests(unittest.TestCase):
    
    def test_pattern_base_class(self):
        pattern = Pattern(None, **{
            "paramA": "a"
        })

        # Check we can get 
        self.assertEqual(pattern.paramA, "a")

        # Check we can set
        pattern.paramA = 1
        self.assertEqual(pattern.paramA, 1)

        # Check we can access parameter dictionary
        self.assertEqual(pattern.params["paramA"], pattern.paramA)
