import unittest
from mock import patch, MagicMock, Mock
import testUtils
import os
from shutil import copyfile

import patternGenerator.settings as default_settings
from patternGenerator.experimentation.trial import Trial
from patternGenerator.experimentation.trial import deserialise
from patternGenerator.experimentation.trial import save_to_video
from patternGenerator.experimentation.trial import load_from_video

from patternGenerator.skin.cell import Cell
from patternGenerator.skin.membrane import Membrane
import patternGenerator.skin.dimensions as dimensions
from patternGenerator.skin.pull import Pull

import subprocess


# Test the pattern Generator!!
class TrialTests(unittest.TestCase):
    def setUp(self):
        self.test_video = os.path.dirname(os.path.realpath(__file__)) + "/" + "test_experiment_video.mp4"

        copyfile(testUtils.video_path, self.test_video)
        self.sha = "ABC123"

    def test_serialisation(self):
        # Make a brane
        t = 0
        centroid = [1, 0]
        d = 1
        state = 1
        framerate = 1
        voltage = 0
        cells = [
            Cell(centroid, d, state),
        ]

        pull = Pull(1,2)

        membrane = Membrane(cells, dimensions.load(), pull=pull)
        membrane.dimensions["num_cells"] = len(cells)

        # Make a small experiment
        experiment = Trial({}, default_settings.load(), voltage, membrane, self.sha)
        experiment.add_sample(t, membrane, framerate, voltage)

        # Serialise and Deserialise
        loaded_experiment = deserialise(experiment.serialise())

        # Check the data
        self.assertEqual(loaded_experiment.voltage, voltage)
        self.assertEqual(loaded_experiment.samples[t].framerate, framerate)
        self.assertEqual(loaded_experiment.samples[t].voltage, voltage)

        self.assertEqual(loaded_experiment.sha, self.sha)

        # TODO: check membrane
        loaded_pull = loaded_experiment.samples[0].membrane.pull

        self.assertEqual(loaded_pull.initial_position[0], pull.initial_position[0])
        self.assertEqual(loaded_pull.initial_position[1], pull.initial_position[1])
        self.assertEqual(loaded_pull.position[0], pull.position[0])
        self.assertEqual(loaded_pull.position[1], pull.position[1])

        # Check cells
        cell = loaded_experiment.samples[t].membrane.cells[0]

        self.assertTrue(all(cell.centroid == centroid))
        self.assertTrue(all(cell.initial_centroid == centroid))
        self.assertEqual(cell.initial_diameter, d)
        self.assertEqual(cell.diameter, d)
        self.assertEqual(cell.state, state)

    def test_metadata_embedding(self):
        membrane = Membrane([], dimensions.load())
        experiment = Trial({}, default_settings.load(), 0, membrane, self.sha)

        save_to_video(self.test_video, experiment)
        loaded_experiment = load_from_video(self.test_video)

        self.assertTrue(isinstance(loaded_experiment, Trial))

    def tearDown(self):
        os.remove(self.test_video)


if __name__ == '__main__':
    unittest.main()
