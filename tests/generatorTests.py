import unittest
from mock import patch, MagicMock, Mock
import numpy as np
from scipy.misc import imread

import os
import sys
sys.modules['flycapture2'] = Mock()

import patternGenerator.settings as default_settings

from patternGenerator.generator import Generator
from patternGenerator.experimentation.experiments.patterns.pattern import Pattern
import testUtils

# Test the pattern Generator!!
class TestGenerator(unittest.TestCase):
    def setUp(self):
        # Mocked PointGreyCamera
        self.camera_patcher = patch("patternGenerator.generator.PointGreyCamera")
        camera_mock = self.camera_patcher.start()

        # The mock camera returns a still test image
        camera_mock.return_value.get_frame.return_value = testUtils.test_image()

        # Mocked arduino
        self.arduino_patcher = patch("patternGenerator.generator.ArduinoInterface")
        arduino_mock = self.arduino_patcher.start()
        arduino_mock.return_value.get_voltage.return_value = 0

        # Mock a pattern
        self.pattern_patcher = patch("patternGenerator.experimentation.experiments.patterns.pattern.Pattern")
        pattern_mock = self.pattern_patcher.start()
        Pattern.update = MagicMock()

    # Check that we can generate an experiment
    def test_generator(self):
        settings = default_settings.load()
        settings["voltage_server"]["ip"] = "127.0.0.1"

        pattern_generator = Generator(settings)
        
        # Check we can make something we can serialise
        trial = pattern_generator.run(Pattern(None), 0.1, pre_sleep_time=0)
        trial.serialise()

        # Check that the server is stil running
        self.assertTrue(pattern_generator.voltage_server.isAlive())


    def tearDown(self):
        self.camera_patcher.stop()
        self.arduino_patcher.stop()


if __name__ == '__main__':
    unittest.main()
    