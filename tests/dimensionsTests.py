import unittest
import patternGenerator.skin.dimensions as dimensions

class TestDimensions(unittest.TestCase):
    def test_default_skin_dimensions(self):
        """
            Make sure we can get a default dict
        """
        default_settings = dimensions.load()
        self.assertTrue(default_settings, dict)
