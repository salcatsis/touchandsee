import unittest
from mock import patch, MagicMock, Mock

# Globally mock flycapture2 imports
import sys
sys.modules['flycapture2'] = Mock()

from tests.trialTests import TrialTests
from tests.generatorTests import TestGenerator
from tests.trackerTests import TestTracker
from tests.dimensionsTests import TestDimensions
from tests.calibratorTests import CalibratorTests
from tests.patternTests import PatternTests
from tests.testExperiments import TestExperiments

import unittest

#TODO Clean build

def main():
    # Test List
    tests = [
        TestTracker,
        TrialTests,
        TestGenerator,
        TestDimensions,
        CalibratorTests,
        PatternTests,
        TestExperiments
    ]

    # Load 'em up
    for test in tests:
        unittest.TestLoader().loadTestsFromTestCase(test)

    unittest.main()

if __name__ == '__main__':
    main()