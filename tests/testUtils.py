import os
import subprocess
from scipy.misc import imread
import numpy as np

basepath = os.path.dirname(__file__) + "/"
image_path = basepath + "test_cells.png"
video_path = basepath + "test_video.mp4"

# Uses ffmpeg to make a video of am image
def make_test_movie(self, filename):
    # Check if file exists
    if os.path.isfile(filename):
        os.remove(filename)

    subprocess.call([
        "ffmpeg",
        "-loop", "1",
        "-i", testVars.image_path,
        "-t", "5",
        "-r", "1",
        "-c:v", "libx264",
        filename
    ])

    return filename

def test_image():
    return imread(image_path)[:,:,0:3].astype(np.uint8)
