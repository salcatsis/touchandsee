import unittest
import testUtils
import patternGenerator.settings as settings
from patternGenerator.tracker import Tracker as Tracker
import patternGenerator.skin.dimensions as dimensions
import testUtils

class TestTracker(unittest.TestCase):
    def setUp(self):
        self.test_image = testUtils.test_image()
        self.crop_radius = 750
        self.n_cells = 5
        self.settigs = settings.load()

    def dimensions(self):
        dims = dimensions.load()
        dims["num_cells"] = self.n_cells
        return dims

    def test_image(self):
        # The test image has 5 cells on it, the tracker should be find up to 5
        # TODO: Check the data makes sense
        for i in range(self.n_cells+1):
            dims = self.dimensions()
            dims["num_cells"] = i
            tracker = Tracker(self.test_image, dims, crop_radius=self.crop_radius)

        # TODO: What happens if more cells are expected than detected?

    def test_crop_center(self):
        # Crops if the circle is out of the image
        tracker = Tracker(self.test_image, 
            self.dimensions(),
            crop_radius=self.crop_radius,
            center=(0,0)
        )

        # Resets center if it is out of range
        # Crops if the circle is out of the image
        tracker = Tracker(self.test_image, 
            self.dimensions(),
            crop_radius=self.crop_radius,
            center=(-10,-10)
        )

        self.assertTrue(tracker.center[0]==0)
        self.assertTrue(tracker.center[1]==0)

        # Resets center if it is out of range
        # Crops if the circle is out of the image
        tracker = Tracker(self.test_image, 
            self.dimensions(),
            crop_radius=self.crop_radius,
            center=(float("inf"), float("inf"))
        )
        self.assertTrue(tracker.center[0]==self.test_image.shape[0])
        self.assertTrue(tracker.center[1]==self.test_image.shape[1])

        # Handles floats
        tracker = Tracker(self.test_image, 
            self.dimensions(),
            crop_radius=self.crop_radius,
            center=(1.1, -2.3)
        )

    
    def tearDown(self):
        pass



if __name__ == '__main__':
    unittest.main()