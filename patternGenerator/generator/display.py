import threading
import cv2
import numpy as np
import patternGenerator.utils as utils

class Display(threading.Thread):
    """
        A display to show to the user
    """
    def __init__(self, framerate=20, image_height=400):
        # Initialise Thread
        threading.Thread.__init__(self)
        self.daemon=True
        self.lock = threading.Lock()

        # Frame settings
        self.frame = None
        self.period = int(1000.0/framerate)
        self.image_height = image_height

        # The state of the cells
        self.states = []

        self.start()
    
    def run(self, window_name="Display"):
        while True:
            # Display to user
            # TODO: initialisation dims?
            if self.frame is None:
                continue

            # Color the Frame
            resized = utils.resize(cv2.cvtColor(self.frame, cv2.COLOR_RGB2BGR), self.image_height)
            
            # Get dimensions
            height, width = resized.shape[:2]
            
            # Initialise the info bar
            radius = int(float(width)/(1 + 3*len(self.states)))
            info_height = 3*radius
            info_bar = 255*np.ones([info_height, width, 3], dtype="uint8")
            
            # Populate info bar with circles
            center = [int(info_height/2.0), int(1.5*radius)]
            for state in self.states:
                color = [0,255,0] if state else [0,0,255]
                cv2.circle(info_bar, tuple(center), radius, color, -1)

                center[0] += 3*radius

            # Concatenate images and show to user
            combined = np.vstack([resized, info_bar])

            cv2.imshow(window_name, combined)
            if cv2.waitKey(self.period) == ord('q'):
                break


    def update(self, frame, states):
        with self.lock:
            self.frame = frame
            self.states = states
