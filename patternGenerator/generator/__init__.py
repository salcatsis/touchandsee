## Imports

import skvideo.io
import numpy as np
import cv2
import argparse
import time
import sys, select, os
from matplotlib import pyplot as plt

from patternGenerator.comms.cameras.pointGreyCamera import PointGreyCamera

from patternGenerator.tracker import Tracker

from patternGenerator.skin.membrane import Membrane
from patternGenerator.skin.cell import Cell
import patternGenerator.skin.dimensions as default_dimensions

from patternGenerator.experimentation.trial import Trial
from patternGenerator.experimentation.trial import save_to_video
from patternGenerator.comms.arduinoInterface import ArduinoInterface
from patternGenerator.comms.voltageServer import VoltageServer

from display import Display
import patternGenerator.settings as default_settings

import git

class Generator:
    """
        The pattern generator performs a single experiment
    pattern: pattern used to run the experiment
    settings: settings to calibrate the algorithms
    number_of_cells: how many cells to expect
    filename: where to save the file
    voltage: voltage used to actuate the cells (for reference)
    """

    # TODO - automate number of cells...
    def __init__(self,
            settings=None,
            voltage=0,
            dimensions=None, #TODO
        ):
        # Experiment settings
        self.settings = settings if settings else default_settings.load()

        # Default dimensions if none is defined
        if dimensions is None:
            dimensions = default_dimensions.load()

        # TODO: Exception Handling
        self.arduino = ArduinoInterface(**self.settings["arduino"])

        self.voltage_server = VoltageServer(**self.settings["voltage_server"])

        self.camera = PointGreyCamera(**self.settings["camera"])
        self.camera.start(None)

        # Initialise Tracker
        self.tracker = Tracker(
            self.get_frame_from_stream(), 
            dimensions,
            **self.settings["tracker"]
        )

        self.camera.stop()

        self.voltage = voltage

        if self.settings["pattern_generator"]["show_display"]:
            self.display = Display(**self.settings["pattern_generator"]["display"])
        else:
            self.display = None

    def membrane(self):
        """
            Shadow reference for membrane
        """
        return self.tracker.membrane

    def run(self, pattern, pattern_time=1, filename=None, pre_sleep_time=0):
        """
            Runs a pattern for a specified time
        """
        self.arduino.send_states([False for cell in self.membrane().cells])
        
        # Check filename
        if filename is not None and os.path.isfile(filename):
            answer = raw_input("File exists, overwrite?")
            if not (answer == "yes" or answer=="y"):
                print "Exiting..."
                return

            os.remove(filename)


        # Wait for the next experiment to start
        wakeup_time = time.time() + pre_sleep_time
        while time.time() < wakeup_time:
            print "\rSleeping before experiment, starting in %is" % (wakeup_time - time.time()), 
            sys.stdout.flush()
            time.sleep(1)

        self.camera.start(filename)

        # Set all cell states to 0
        # TODO: Wait time for relaxation
        for cell in self.tracker.membrane.cells:
            cell.state = 0

        # Initialise Experiment
        trial = Trial(
            pattern.info(), 
            self.settings, 
            self.voltage,
            self.membrane(),
            git.Repo(search_parent_directories=True).head.object.hexsha
        )

        # Main Execution Loop
        start_time = time.time()
        pattern.start()

        while time.time() - start_time < pattern_time:
            # For framrate info
            loop_start = time.time()
            elapsed_time = time.time() - start_time

            # Display text
            print "\r%.1f" % (time.time() - start_time), "of", pattern_time,
            sys.stdout.flush()

            # Get a frame from the camera
            frame = self.get_frame_from_stream()

            # Update Membrane state and pattern
            self.tracker.process(frame)
        
            # Return an int to sweep
            # TODO: Clean this up
            sweep = pattern.update(elapsed_time)

            if sweep is not None and len(sweep) == 2:
                sweep_angle, sweep_time = sweep

            if sweep == "default":
                sweep_angle = "default"
                sweep_time = "default"

            if sweep:
                # TODO: HACK Clean up pattern motor signals
                if (sweep[0] == 'h'):
                    hold_angle = sweep[1]
                    hold_time = sweep[2]
                    self.arduino.hold(hold_angle, hold_time)
                else:
                    sweep_angle = self.settings["pattern_generator"]["default_sweep_angle"] if sweep_angle == "default" else sweep_angle
                    sweep_time = self.settings["pattern_generator"]["default_sweep_time"] if sweep_time == "default" else sweep_time
                    self.arduino.sweep(sweep_angle, sweep_time)

            self.arduino.send_states([cell.state for cell in self.tracker.membrane.cells])

            # Update Display
            if self.display:
                self.display.update(frame, [cell.state for cell in self.tracker.membrane.cells])

            # TODO: Send to Arduino
            framerate = 1.0 / (time.time() - loop_start)
            trial.add_sample(
                elapsed_time, 
                self.tracker.membrane, 
                framerate,
                self.voltage_server.get_voltage()
            )

        # Cleanup
        self.arduino.send_states([False for cell in self.membrane().cells])
        self.camera.stop()

        return trial

    # Get frame from live stream
    def get_frame_from_stream(self):
        return self.camera.get_frame()


# This app runs a single experiment
# TODO: dimension loading
def main(args):
    # Get printout of available patterns
    description = "available Patterns:\n    "
    description += "\n    ".join(patternList.patternDict.keys())

    # Construct the arguments and parse
    argument_parse = argparse.ArgumentParser(description=description, formatter_class=argparse.RawTextHelpFormatter)

    argument_parse.add_argument('-v', '--voltage',
        help="voltage used to actuate cells",
        type=int,
        default=0,
    )

    argument_parse.add_argument('-f', '--filename',
        help="filename of video to save the experiment"
    )

    argument_parse.add_argument('-p', '--pattern',
        help="pattern choice",
        default="",
    )

    argument_parse.add_argument('-t', '--pattern-time',
        help="how long to run the pattern for",
        type=float,
        default=3.0,
    )

    # Parse the args
    args = argument_parse.parse_args()

    # Check the pattern
    if args.pattern not in patternList.patternDict:
        print "UNKNOWN PATTERN", args.pattern
        quit()

    # Perform the experiment
    generator = Generator(
        default_settings.load(),
        voltage=args.voltage
    )

    # Construct the default pattern
    experiment = generator.run(
        patternList.patternDict[args.pattern](generator.membrane()), 
        args.pattern_time,
        filename = args.filename
    )

    # Save data generated from experiment.
    if args.filename:
        save_to_video(args.filename, experiment)
