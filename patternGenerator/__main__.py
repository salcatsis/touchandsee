#!/usr/bin/python

import argparse
import sys
import tracker.calibrator
import comms.voltageServer
import experimentRunner
import comms.cameras.pointGreyCamera as camera


parser = argparse.ArgumentParser(
    description='Smart Squid Skin Experimentation', 
    formatter_class=argparse.RawTextHelpFormatter,
    add_help=False,
    prefix_chars=" " # Dummy to remove help prefix
)

# Subparser Choices
parser.add_argument("mode", help="Choose your mode", nargs="?", default=None, choices=[
        "calibrate",
        "voltage-monitor",
        "experiment",
        "pattern",
        "plotter",
        "camera",
    ], 
)

# Parse
try:
    known_args, remaining_args = parser.parse_known_args(sys.argv[1:])

except:
    parser.print_help()
    quit()

# pass onto subparser
if known_args.mode == "calibrate":
    tracker.calibrator.main(remaining_args)

elif known_args.mode == "voltage-monitor":
    comms.voltageServer.main(remaining_args)

elif known_args.mode == "experiment":
    experimentRunner.main(remaining_args)

elif known_args.mode == "pattern":
    experimentRunner.pattern_runner(remaining_args)

elif known_args.mode == "plotter":
    experimentRunner.plotter(remaining_args)

elif known_args.mode == "camera":
    camera.main(remaining_args)

else:
    parser.print_help()


