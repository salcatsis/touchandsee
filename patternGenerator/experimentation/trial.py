import json

from . import Cell
from . import Membrane
from . import dimensions
from . import Pull

import argparse
from sample import Sample
import os
import mutagen
import numpy as np


# TODO: Find another way that allows appending to hard drive
# Data format for saving experiment data
class Trial:
    def __init__(self, pattern_info, settings, voltage, membrane, sha):
        self.pattern_info = pattern_info
        self.settings = settings
        self.voltage = voltage
        self.membrane = membrane
        self.sha = sha

        self.samples = {}

    def num_cells(self):
        return self.membrane.dimensions["num_cells"]

    # Add a sample
    def add_sample(self, t, membrane, framerate, voltage):
        if len(membrane.cells) != self.num_cells():
            raise Exception(str(self.num_cells()) + " cells expected in Experiment. " + str(len(membrane.cells)) + " received")

        # TODO: Better compression on data format?
        self.samples[t] = Sample(membrane, framerate, voltage)

    # Sorted ascendingly
    def timestamps(self):
        return sorted(self.samples.keys())

    # Serialise into JSON
    def serialise(self):
        return json.dumps(self.experiment_dict(), separators=(',', ':'))

    def membrane_dict(self):
        """
            Dictionary to represent a reference membrane (with data common between samples)
        """
        return {
            "dimensions": self.membrane.dimensions, # TODO
            "cells": [{
                "initial_centroid": cell.initial_centroid.tolist(),
                "initial_diameter": cell.initial_diameter
            } for cell in self.membrane.cells],
            "pull": {
                "initial_position": self.membrane.pull.initial_position.tolist(),
            }
        }

    def sample_dict(self):
        """
            Dictionary to represent samples
        """
        dict_samples = {}
        for t, sample in self.samples.iteritems():
            cell_dict = [{
                "id": i,
                "state": cell.state,
                "centroid": cell.centroid.tolist(),
                "diameter": cell.diameter,
            } for i, cell in enumerate(sample.membrane.cells)]

            dict_samples[t] = {
                "membrane": cell_dict,
                "pull": {"position": sample.membrane.pull.position.tolist()},
                "framerate": sample.framerate,
                "voltage": sample.voltage
            }

        return dict_samples      

    def experiment_dict(self):
        """
            JSON Friendly Trial Dictionary
        """ 

        return {
            "pattern_info": self.pattern_info,
            "voltage": self.voltage,
            "settings": self.settings,
            "samples": self.sample_dict(),
            "membrane": self.membrane_dict(),
            "sha": self.sha
        }

    def convert_to_mm(self, x):
        """
            Converts to mm
        """

        factor = float(self.settings["pattern_generator"]["mm_per_pixel"])

        if hasattr(x, "__len__"):
            x = np.array(x)

        return x*factor

    def cell_stretches(self):
        """
            A num_cells x t array
        """
        cell_stretches = []

        for t in self.timestamps():
            stretches = [cell.stretch_ratio() for cell in self.samples[t].membrane.cells]
            cell_stretches.append(stretches)

        return np.array(cell_stretches)

    def pull_positions(self):
        cell_stretches = []

        X = []
        Y = []
        for t in self.timestamps():
            x, y = self.samples[t].membrane.pull.position
            X.append(x)
            Y.append(y)

        return X,Y


# Create an experiment object form data
def deserialise(json_string):
    # Raw data
    data_dict = json.loads(json_string)
    
    # Make an example membrane
    cells = [Cell(
        cell["initial_centroid"],
        cell["initial_diameter"]
        ) for cell in data_dict["membrane"]["cells"]
    ]

    # HACK: Migrate data to have pull fields
    if "pull" in data_dict["membrane"]:
        pull = Pull(*data_dict["membrane"]["pull"]["initial_position"])
    else:
        pull = Pull()

    membrane = Membrane(cells, data_dict["membrane"]["dimensions"], pull=pull)

    # Initialise experiment from metadata
    experiment = Trial(
        data_dict["pattern_info"], 
        data_dict["settings"], 
        data_dict["voltage"],
        membrane,
        data_dict["sha"]
    )

    # Sample dictionary of samples keyed by time
    reference_cells = data_dict["membrane"]["cells"]
    for t, sample in data_dict["samples"].iteritems():
        cells = []
        for i, cell_dict in enumerate(sample["membrane"]):
            cell = Cell(reference_cells[i]["initial_centroid"], float(reference_cells[i]["initial_diameter"]))
            cell.state = bool(cell_dict["state"])
            cell.update(cell_dict["centroid"], float(cell_dict["diameter"]))
            cells.append(cell)

        # HACK: Migrate data to have pull fields
        if "pull" in data_dict["membrane"]:
            sample_pull = Pull(*data_dict["membrane"]["pull"]["initial_position"])
            sample_pull.set_position(*sample["pull"]["position"])
        else:
            sample_pull = Pull()

        experiment.add_sample(float(t), 
            Membrane(cells, membrane.dimensions, pull=sample_pull), 
            sample["framerate"],
            sample["voltage"],
        )

    return experiment

def save_to_video(filename, experiment):
    # Check if file exists
    if not os.path.isfile(filename):
        raise Exception(str(filename) + " doesn't exist for metadata attachment")

    file = mutagen.File(filename)

    file["desc"] = [experiment.serialise()]
    file.save()

def load_from_video(filename):
    file = mutagen.File(filename)
    json_string = file["desc"][0]
    return deserialise(json_string)


# Demo Data
if __name__ == '__main__':
    # Construct the arguments and parse
    argument_parse = argparse.ArgumentParser(description="Print info on an mp4 experiment file")
    argument_parse.add_argument('filename', help="File to read")
    argument_parse.add_argument('-a', '--all', help="Print entire experiment json", action="store_true")

    args = argument_parse.parse_args()

    # Load experiment
    experiment_dict = load_from_video(args.filename).experiment_dict()
    
    # Nicer for printing to screen
    if experiment_dict["samples"] and not args.all:
        del experiment_dict["samples"]
        print "Metadata for", args.filename
        print json.dumps(experiment_dict, indent=4)
    else:
        print json.dumps(experiment_dict, indent=4)
