from patternGenerator.skin.membrane import Membrane
from patternGenerator.skin.cell import Cell
from patternGenerator.skin.pull import Pull
from patternGenerator.skin import dimensions
from patternGenerator.generator import Generator

import patternGenerator.settings as settings

import glob
import os

import sys, inspect


def main(args):
    # Parse args
    argument_parse = argparse.ArgumentParser(description="run an experiment")

    argument_parse.add_argument('mode',
        help="run/analyse"
    )

    argument_parse.add_argument('experiment', 
        help="name of experiment" 
    )
    
    argument_parse.add_argument('directory',
        help="directory"
    )


    args = argument_parse.parse_args(args)

    # Get requested experiment
    experiments = {}
    for name, member in inspect.getmembers(experimentList, inspect.isclass):
        if issubclass(member, Experiment):
            experiments[name] = member

    if not (args.experiment in experiments):
        raise Exception("Unknown experiment " + args.experiment)

    experiment = experiments[args.experiment](args.directory)

    # Run it!
    if args.mode == 'run':
        experiment.run()

    elif args.mode == 'analyse':
        experiment.analyse()

