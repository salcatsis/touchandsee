import trial
import mutagen
import json
import sys

if __name__ == '__main__':
    filename = sys.argv[1]

    file = mutagen.File(filename)
    raw_json = json.loads(file["desc"][0])

    raw_json["sha"] = "NONE"

    reserialised = json.dumps(raw_json, separators=(',', ':'))

    file["desc"] = [reserialised]

    file.save()

