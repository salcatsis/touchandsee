class Sample:
    """
        Data Sample in an experiment
    """
    def __init__(self, membrane, framerate, voltage):
        self.membrane = membrane.copy()
        self.framerate = framerate
        self.voltage = voltage
