from experiment import Experiment

from patterns.passingCloudPattern import PassingCloudPattern

class PassingCloudExperiment(Experiment):
    def run(self, voltage=3000, pattern_time=50):
        generator = Generator(voltage=voltage)

        filename = self.directory + 'passing_cloud.mp4'

        trial = generator.run(
            PassingCloudPattern(generator.membrane()),
            pattern_time=pattern_time,
            filename=filename
        )

        self.save(filename, trial)

    # TODO: ANALYSIS

if __name__ == '__main__':
    experiment = PassingCloudExperiment(sys.argv[1])
    experiment.analyse()
