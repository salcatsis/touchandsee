import glob
import os

from . import load_from_video
from . import save_to_video

# TODO: init experiment params
class Experiment(object):
    """
        An experiment defines a set of trials and their associated analysis
    """

    # Don't forget to define the directory!
    def __init__(self, directory, pre_sleep_time=300):
        self.pre_sleep_time = pre_sleep_time
        self.directory = directory
        self.trial_num = 1

    def run(self, directory):
        raise NotImplementedError("Run function not defined in experiment")

    def analyse(self):
        raise NotImplementedError("analyse function not defined in experiment")

    def load(self):
        files = sorted(glob.glob(self.directory+"*.mp4"))
        return [load_from_video(file) for file in files]

    def save(self, filename, trial):
        save_to_video(filename, trial)

    # Runs an experiment
    def start(self, generator):
        # TODO: Generator kwargs 
        self.run(generator, pre_sleep_time=self.pre_sleep_time)

    # Performs the next experiment
    def next_trial(self, generator, pattern, **kwargs):
        filename = self.directory + "trial%i.mp4"%self.trial_num
        print "\n", "*"*30
        print "Trial %s: %s" % (self.trial_num, os.path.basename(filename))
        
        trial = generator.run(pattern, 
            filename=filename, 
            pre_sleep_time=self.pre_sleep_time,
            **kwargs
        )
        
        #TODO: Wait for long time limit

        save_to_video(filename, trial)


        self.trial_num += 1
