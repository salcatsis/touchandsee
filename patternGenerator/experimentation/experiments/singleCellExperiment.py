from experiment import Experiment

# TODO: Move this relative link to __init__
from patterns.singleCellPattern import SingleCellPattern

import sys
import numpy as np
import matplotlib.pyplot as plt


class SingleCellExperiment(Experiment):
    """
        Loop over each cell in turn on
        on_time: how long to be before turning off
    """
    def run(self, generator, on_time=20, pre_sleep_time=300):
        self.pre_sleep_time = pre_sleep_time

        # Loop over each one and run
        for i in range(5):
            # Construct the pattern
            self.next_trial(
                generator,
                SingleCellPattern(generator.membrane(), 
                    on_time=on_time, 
                    cell_index=i
                ), 
                pattern_time=2*on_time
            )

    def analyse(self):
        trials = self.load()

        cell0compare = []
        for trial_index, trial in enumerate(trials):
            timestamps = trial.timestamps()

            cell_stretches = trial.cell_stretches()
            plt.plot(timestamps, cell_stretches[:, trial_index], label="Cell %i" % (trial_index+1))

        plt.xlabel("Time (s)")
        plt.ylabel("Stretch Ratio")
        plt.title("A Single turning on")
        plt.legend()
        plt.grid()

        plt.show()

if __name__ == '__main__':
    experiment = SingleCellExperiment(sys.argv[1])
    experiment.analyse()
