from experiment import Experiment

import sys
import matplotlib.pyplot as plt

from patterns.nothingPattern import NothingPattern

import plotUtils

class StillExperiment(Experiment):
    """
        Doesn't do anything, just gain metrics
    """
    def run(self, run_time=30):
        # TODO: Check if directory exists and DRY
        generator = self.generator(voltage=0)

        pattern = NothingPattern(generator.membrane())

        filename = self.directory + "trial.mp4"
        self.save(filename, generator.run(pattern, pattern_time=run_time, filename=filename))


    def analyse(self):
        trial = self.load()[0]
        
        plotUtils.diameter_distribution(trial)
        plt.show()   


if __name__ == '__main__':
    stillExperiment = StillExperiment(sys.argv[1])
    stillExperiment.analyse()
