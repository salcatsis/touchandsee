from experiment import Experiment
from patterns.differencePattern import DifferencePattern

import numpy as np

class DifferenceExperiment(Experiment):

    def run(self, generator, start=-0.3, stop=0, num=5, pre_sleep_time=300, pattern_time=30):
        self.pre_sleep_time=pre_sleep_time

        thresholds = np.linspace(start, stop, num)
        for threshold in thresholds:
            pattern = DifferencePattern(generator.membrane, off_threshold=threshold)
            self.next_trial(generator, pattern, pattern_time=pattern_time)

    def analyse(self):
        trials = self.load()
