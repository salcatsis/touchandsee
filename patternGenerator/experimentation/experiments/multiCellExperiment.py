from experiment import Experiment

import sys
import matplotlib.pyplot as plt

class MultiCellExperiment(Experiment):
    """
        Turn on 1, 2, ... cells all at once
    """
    def run(self, generator, voltage=3000, on_time=20):
        generator = Generator(voltage=voltage)
    
        cell_indices = []
        for i in range(5):
            cell_indices.append(i)

            self.next_trial(generator,
                MultiCellPattern(generator.membrane(), 
                    on_time=on_time, 
                    on_cell_indices=cell_indices
                ),
                pattern_time=2*on_time,
            )


    def analyse(self):
        trials = self.load()

        cell0compare = []
        for trial_index, trial in enumerate(trials):
            timestamps = trial.timestamps()

            cell0stretch = []
            for t in timestamps:
                cell0stretch.append(trial.samples[t].membrane.cells[0].stretch_ratio())

            label = str(trial_index+1) + " cells on"
            plt.plot(timestamps, cell0stretch, label=label)

        plt.xlabel("Time (s)")
        plt.ylabel("Stretch Ratio")
        plt.grid()
        plt.legend()

        plt.show()

if __name__ == '__main__':
    experiment = MultiCellExperiment(sys.argv[1])
    experiment.analyse()
