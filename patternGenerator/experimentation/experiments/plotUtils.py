import argparse
import matplotlib.pyplot as plt
import math

from . import trial
import pprint 
import numpy as np

def new_axes():
    fig = plt.figure()
    ax = fig.add_subplot(111)
    return fig, ax

# TODO: Change to strain plot
def diameter_plot(trial):
    figure, axes = new_axes()

    # Times of the experiments
    times = trial.timestamps()

    # Get Diameters
    diameters = []
    for t in times:
        cell_dims = [cell.stretch_ratio() for cell in trial.samples[t].membrane.cells]
        diameters.append(cell_dims)

    diameters = np.array(diameters)

    # Plot Diameters
    x = np.arange(diameters.shape[1]+1) + 0.5
    y = times + [times[-1]+0.01]
    cax = plt.pcolor(y, x, np.transpose(diameters), cmap='gray')
    
    colorbar = figure.colorbar(cax)
    colorbar.ax.set_ylabel("Stretch Ratio")
    #plt.clim(0, int(math.ceil(np.max(diameters) / 10.0)) * 10)
    axes.set_xlabel("Time (s)")
    axes.set_ylabel("Cell")    

def diameter_plot_lines(trial):
    figure, axes = new_axes()

    # Times of the experiments
    times = trial.timestamps()

    # Get Diameters
    diameters = []
    for t in times:
        cell_dims = [cell.stretch_ratio() for cell in trial.samples[t].membrane.cells]
        diameters.append(cell_dims)

    diameters = np.array(diameters)

    # Plot Diameters
    x = np.arange(diameters.shape[1]+1) + 0.5
    y = times + [times[-1]+0.01]

    for i in range(diameters.shape[1]):
        axes.plot(times, diameters[:,i], label="Cell %i"%(i+1))

    #plt.clim(0, int(math.ceil(np.max(diameters) / 10.0)) * 10)
    axes.grid()
    axes.legend()
    axes.set_xlabel("Time (s)")
    axes.set_ylabel("Stretch Ratio") 


def state_plot(trial):
    fig, ax = new_axes()

    # Times of the experiments
    times = trial.timestamps()

    # Get Diameters
    states = []
    for t in times:
        cell_states = [cell.state for cell in trial.samples[t].membrane.cells]
        states.append(cell_states)

    states = 1 - np.array(states)

    # Plot Diameters
    x = np.arange(states.shape[1]+1) + 0.5
    y = times + [times[-1]+0.01]
    cax = plt.pcolor(y, x, np.transpose(states), cmap='gray')

    ax.plot([], [], "ko", label="Cell On")
    ax.plot([], [], "wo", label="Cell Off")
    ax.legend()
    # colorbar = fig.colorbar(cax, cmap='greys')
    # colorbar.ax.set_ylabel("State")
    #plt.clim(0, int(math.ceil(np.max(diameters) / 10.0)) * 10)
    ax.set_xlabel("Time (s)")
    ax.set_ylabel("Cell")        


# ok for now
def centroid_plot(trial):
    fig, ax = new_axes()

    # Get Centroids
    cell_centroids = []
    for t, sample in trial.samples.iteritems():
        cell_centroids.append([cell.centroid for cell in sample.membrane.cells])

    cell_centroids = trial.convert_to_mm(np.array(cell_centroids))

    # Plot Centroids as a path
    for i in range(cell_centroids.shape[1]):
        ax.plot(cell_centroids[:,i,0], cell_centroids[:,i,1], 'rx', linewidth=1)

    # TODO: Plot boundary

    ax.set_xlabel("Distance (mm)")
    ax.set_ylabel("Position (mm)")

    radius = trial.convert_to_mm(trial.settings["tracker"]["crop_radius"])
    center = trial.convert_to_mm(np.array(trial.settings["tracker"]["center"]))
    ax.set_xlim(center[0] - radius, center[0] + radius)
    ax.set_ylim(center[1] - radius, center[1] + radius)

def diameter_distribution(trial):
    fig, ax = new_axes()

    # Get diameters
    diameters = trial.convert_to_mm(np.array([cell.initial_diameter for cell in trial.membrane.cells]))

    ax.grid()
    ax.scatter(diameters, [0]*len(diameters))
    ax.yaxis.set_visible(False)
    ax.set_xlabel("Diameter (mm)")
    
def pull_plot(trial):
    fig, ax = new_axes()

    x, y = trial.convert_to_mm(trial.pull_positions())

    centers = trial.convert_to_mm(trial.settings["tracker"]["center"])
    x -= centers[0]
    y -= centers[1]

    circle_color = 'r'
    circle_radius = trial.convert_to_mm(trial.settings["tracker"]["crop_radius"])
    circle = plt.Circle((0, 0), circle_radius, color=circle_color, fill=False)
    ax.plot([], [], circle_color, label="Membrane")


    ax.plot(x, y, label="String Motion")
    ax.add_artist(circle)

    padding=1.5
    ax.grid()
    ax.legend()
    ax.set_xlim([-circle_radius*padding, circle_radius*padding])
    ax.set_ylim([-circle_radius*padding, circle_radius*padding])
    ax.set_xlabel("x (mm)")
    ax.set_ylabel("y (mm)")
    ax.set_aspect('equal', 'datalim')

def framerate_plot(trial):
    fig,ax = new_axes()

    t = trial.timestamps()
    framerates = [trial.samples[T].framerate for T in t]

    ax.plot(t, framerates)
    ax.set_xlabel("Time (s)")
    ax.set_ylabel("Framerate (Hz)")

def plot(trial):
    state_plot(trial)
    diameter_plot(trial)
    framerate_plot(trial)
    centroid_plot(trial)
    diameter_distribution(trial)

    plt.show()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument("filename", help="mp4 file of experiment video")
    args = parser.parse_args()

    trial = load_from_video(args.filename)
    plot(trial)



    