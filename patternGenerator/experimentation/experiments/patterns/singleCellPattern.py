from pattern import Pattern
import time

class SingleCellPattern(Pattern):
	"""
		Turns a single cell for a specific amount of time
	"""
	def __init__(self, membrane, cell_index=2, on_time=100):
		Pattern.__init__(self, membrane, **{
			"cell_index": cell_index,
			"on_time": on_time
		})

		if cell_index > (len(membrane.cells) - 1):
			raise Exception("Cell index %i, out of range for %s", self.cell_index, self.name())

		self.trial_cell = self.membrane.cells[cell_index]
		self.off_time = on_time

	def update(self, t):
		# Turn all cells off
		for cell in self.membrane.cells:
			cell.state = False

		# But actually turn the main one on
		if t < self.off_time:
			self.trial_cell.state = True
