from pattern import Pattern
import time
import patternGenerator.experimentation.experiments.plotUtils as plotUtils
import numpy as np

class DiffTriggerPattern(Pattern):
    """
        Cell ahead - Cell in front
    """
    def __init__(self, membrane, 
        on_threshold=0.01, 
        off_threshold=-0.03,
        resweep_time=10.0,
        resweep_period=10.0
    ):
        Pattern.__init__(self, membrane,
            on_threshold=on_threshold,
            off_threshold=off_threshold,
            resweep_time=resweep_time,
            resweep_period=resweep_period
        )

        self.first_switch=False
        self.second_switch=False

    def update(self, t):
        for cell in self.membrane.cells:
            source = DiffTriggerPattern.source(cell)

            # Switching state
            if cell.state and source < self.off_threshold:
                cell.state = not cell.state
            elif not cell.state and source > self.on_threshold:
                cell.state = not cell.state

        if not self.first_switch:
            self.first_switch=True
            return "default"

        if not self.second_switch and t > self.resweep_time:
            self.second_switch = True
            return ("default", self.resweep_period)

    @staticmethod
    def source(cell, weight=1):
        return cell.next_cell.stretch_ratio() - cell.previous_cell.stretch_ratio()


    #TODO: HACK?
    @staticmethod
    def plot(trial):
        fig, ax = plotUtils.new_axes()

        times = trial.timestamps()
        sources=[]
        for t in times:
            sources.append([DiffTriggerPattern.source(cell) for cell in trial.samples[t].membrane.cells])

        sources = np.transpose(sources)

        for i, source in enumerate(sources):
            ax.plot(times, source, label="Cell %i"%(i+1))

        thresh_color='k'
        thresh_thickness=1

        ax.axhline(y=trial.pattern_info["params"]["on_threshold"], linewidth=thresh_thickness, color=thresh_color, linestyle="--")
        ax.axhline(y=trial.pattern_info["params"]["off_threshold"], linewidth=thresh_thickness, color=thresh_color, linestyle="--")


        ax.legend(loc="upper right")
        ax.grid()
        ax.set_xlabel("Time (s)")
        ax.set_ylabel("Source")
        ax.set_title("Source Plot")

