from pattern import Pattern
import time

class PassingCloudPattern(Pattern):

	def __init__(self, 
		membrane, 
		lower_threshold=1.01, 
		upper_threshold=1.018,
		manual_cell_on_time = 0,
		delay=0.0,
		on_delay = 1.3
	):
		Pattern.__init__(self, membrane,
			lower_threshold = lower_threshold,
			upper_threshold = upper_threshold,
			manual_cell_on_time = manual_cell_on_time,
			delay = delay,
			on_delay = on_delay
		)

		self.membrane  = membrane
		self.manual_cell_off_time = 0
		self.last_switch = {cell: 0 for cell in membrane.cells}
		self.last_on_switch = {cell: 0 for cell in membrane.cells}

	def start(self):
		self.manual_cell_off_time = time.time() + self.params["manual_cell_on_time"]

	def update(self, t):
		"""
			Simulate a perturbation 
		"""
		for cell in self.membrane.cells:
			if cell.state and (cell.previous_cell.stretch_ratio() < self.lower_threshold) and ((t - self.last_switch[cell]) > self.delay):
				self.last_switch[cell] = t
				cell.state = False
			
			elif not cell.state and cell.previous_cell.stretch_ratio() > self.upper_threshold and ((t - self.last_on_switch[cell]) > self.on_delay):
				self.last_on_switch[cell] = t
				cell.state = True
