class Pattern(object):
    """
        Interface for a pattern

        kwargs is a dictionary of pattern parameters
    """
    def __init__(self, membrane, **kwargs):
        # Initialise parameter dict
        self.__dict__["params"] = kwargs
        self.membrane = membrane

    def start(self):
        """
            Run just before pattern generation
        """
        pass

    def update(self, t):
        raise Exception('Update not implemented...')

    #TODO: Use __getattribute__ instead?
    def __getattr__(self, name):
        """
            Check parameter dictionary for attribute  before the normal dict,
            that way we can keep track of the kwargs for the pattern
        """
        #TODO: Is there a better way, than an if on every get?
        if not "params" in self.__dict__:
            raise Exception("Need to call base pattern constructor in", self.__class__.__name__)

        if name in self.params:
            return self.params[name]

        raise AttributeError("Could not find attribute", name)

    def __setattr__(self, name, value):
        # TODO: Do we need to check params every time?
        if self.params and name in self.params:
            self.params[name] = value
        else:
            self.__dict__[name] = value

    def name(self):
        return self.__class__.__name__

    # Called when saving the pattern
    def info(self):
        """
            Pattern data used when saving
        :return: dictionary of pattern data
        """
        return {
            "name": self.name(),
            "params": self.params
        }

