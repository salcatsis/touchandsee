from pattern import Pattern
import time
import patternGenerator.experimentation.experiments.plotUtils as plotUtils
import numpy as np

from sources import difference_source
source = difference_source

class TestSingleOscillatorPattern(Pattern):
    """
        Cell ahead - Cell in front
    """
    def __init__(self, membrane, 
        on_threshold=0.025, 
        off_threshold=-0.03,
        hold_time=5
    ):
        Pattern.__init__(self, membrane,
            on_threshold=on_threshold,
            off_threshold=off_threshold,
            hold_time=hold_time
        )

    def update(self, t):
        if t < self.hold_time:        
            for index in [0,1,3]:
                self.membrane.cells[index].state = True
            return

        for cell in self.membrane.cells:
            cell_source = source(cell)

            # Switching state
            if cell.state and source < self.off_threshold:
                cell.state = not cell.state
            elif not cell.state and source > self.on_threshold:
                cell.state = not cell.state


    @staticmethod
    def plot(trial):
        source_plot(trial)
        num_plot(trial)

def num_plot(trial):
    fig, ax = plotUtils.new_axes()

    times = trial.timestamps()
    num_cells_on=[]
    for t in times:
        num_cells_on.append(np.sum([cell.state for cell in trial.samples[t].membrane.cells]))

    num_cells_on = np.transpose(num_cells_on)
    ax.plot(times, num_cells_on)
    ax.set_xlabel("Time (s)")
    ax.set_ylabel("Num Cells on")
    ax.set_title("Number of cells on")


def source_plot(trial):
    fig, ax = plotUtils.new_axes()

    times = trial.timestamps()
    sources=[]
    for t in times:
        sources.append([source(cell) for cell in trial.samples[t].membrane.cells])

    sources = np.transpose(sources)

    for i, s in enumerate(sources):
        ax.plot(times, s, label="Cell %i"%(i+1))

    thresh_color='k'
    thresh_thickness=1

    ax.axhline(y=trial.pattern_info["params"]["on_threshold"], linewidth=thresh_thickness, color=thresh_color, linestyle="--")
    ax.axhline(y=trial.pattern_info["params"]["off_threshold"], linewidth=thresh_thickness, color=thresh_color, linestyle="--")


    ax.legend(loc="upper right")
    ax.grid()
    ax.set_xlabel("Time (s)")
    ax.set_ylabel("Source")
    ax.set_title("Source Plot")
