def difference_source(cell):
    return cell.previous_cell.stretch_ratio() - cell.next_cell.stretch_ratio()
    