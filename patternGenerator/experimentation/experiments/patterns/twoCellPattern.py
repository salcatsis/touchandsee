from pattern import Pattern
import time

class TwoCellPattern(Pattern):

    def __init__(self, 
        membrane, 
        on_threshold=1.05, 
        manual_cell_on_time = 1.5,
        delay=0.5
    ):
        Pattern.__init__(self, membrane,
            on_threshold = on_threshold,
            manual_cell_on_time = manual_cell_on_time,
            delay = delay,
        )

        self.membrane  = membrane
        self.manual_cell_off_time = 0
        self.last_switch = {cell: -float("inf") for cell in membrane.cells}

    def start(self):
        self.manual_cell_off_time = time.time() + self.params["manual_cell_on_time"]

    def update(self):
        """
            Simulate a perturbation 
        """
        for cell in self.membrane.cells:
            num_cells_on = 0

            if cell.previous_cell.stretch_ratio() > self.on_threshold:
                num_cells_on += 1

            if cell.next_cell.stretch_ratio() > self.on_threshold:
                num_cells_on += 1

            cell.state = True if num_cells_on == 1 else False

        if time.time() < self.manual_cell_off_time:
            self.membrane.cells[0].state = True
