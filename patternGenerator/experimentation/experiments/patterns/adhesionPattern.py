from pattern import Pattern
import time


class AdhesionPattern(Pattern):
    def __init__(self, membrane, **kwargs):
        Pattern.__init__(self, membrane)

        self.start_time = time.time()

    def elapsed(self):
        return time.time() - self.start_time

    def update(self):
        if self.elapsed() < 15:
            self.membrane.cells[0].state = True

        if self.elapsed() < 5:
            self.membrane.cells[1].state = False
        elif self.elapsed() < 10:
            self.membrane.cells[1].state = True
        else:
            self.membrane.cells[1].state = False





