from passingCloudPattern import PassingCloudPattern
from manualPattern import ManualPattern
from nonAutonomousCyclePattern import NonAutonomousCyclePattern
from nothingPattern import NothingPattern
from singleCellPattern import SingleCellPattern
from twoCellPattern import TwoCellPattern
from adhesionPattern import AdhesionPattern
from motorPattern import MotorPattern
from differencePattern import DifferencePattern
from diffTriggerPattern import DiffTriggerPattern
from differencePattern2 import DifferencePattern2
from sumPattern import SumPattern
from testSingleOscillatorPattern import TestSingleOscillatorPattern

# A list of default patterns
patterns = [
    PassingCloudPattern,
    ManualPattern,
    NonAutonomousCyclePattern,
    NothingPattern,
    SingleCellPattern,
    AdhesionPattern,
    MotorPattern,
    DifferencePattern,
    DiffTriggerPattern,
    TestSingleOscillatorPattern
]

# A dictionary keyed by the pattern name
patternDict = {}
for pattern in patterns:
    patternDict[pattern.__name__] = pattern