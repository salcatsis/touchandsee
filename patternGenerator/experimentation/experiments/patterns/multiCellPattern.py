from pattern import Pattern
import time

class MultiCellPattern(Pattern):
    """
        Turns a single cell for a specific amount of time
    """
    def __init__(self, membrane, on_cell_indices=None, on_time=100):
        if not on_cell_indices:
            on_cell_indices = []

        Pattern.__init__(self, membrane, **{
            "on_cell_indices": on_cell_indices,
            "on_time": on_time
        })

        for cell_index in on_cell_indices:
            if cell_index > (len(membrane.cells) - 1):
                raise Exception("Cell index %i, out of range for %s", self.cell_index, self.name())

        self.trial_cells = [self.membrane.cells[cell_index] for cell_index in self.on_cell_indices]
        self.off_time = time.time() + on_time

    def update(self, t):
        # Turn all cells off
        for cell in self.membrane.cells:
            cell.state = False

        # But actually turn some on main one on
        if time.time() < self.off_time:
            for cell in self.trial_cells:
                cell.state = True
