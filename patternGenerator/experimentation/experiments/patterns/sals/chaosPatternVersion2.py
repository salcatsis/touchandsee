from pattern import Pattern
import numpy as np 
import time

class ChaosPatternVersion2(Pattern):

	def __init__(self,membrane,lower_threshold,upper_threshold):

		self.cell_membrane  = membrane
		self.lower_threshold = lower_threshold
		self.upper_threshold = upper_threshold


	def update(self):

		count = 0


		for cell in self.cell_membrane.cells:
		

			trigger_signal_anti = cell.anticlockwise_neighbour.get_strain()[0]
			trigger_signal_clock = cell.clockwise_neighbour.get_strain()[0]
			self_strain = cell.get_strain()[0] 
	
			
			if (trigger_signal_anti > self.upper_threshold or trigger_signal_clock > self.upper_threshold) and self_strain <self.upper_threshold:
				cell.state = 1
				cell.growing = True
			elif self_strain > self.upper_threshold:
				cell.growing = False
				cell.state = 0

			if cell.growing:
				cell.state = 1


			count += 1
		




