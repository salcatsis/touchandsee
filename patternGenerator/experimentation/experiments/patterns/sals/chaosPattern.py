from pattern import Pattern
import numpy as np 
import time

class ChaosPattern(Pattern):

	def __init__(self,membrane,lower_threshold,upper_threshold):

		self.cell_membrane  = membrane
		self.lower_threshold = lower_threshold
		self.upper_threshold = upper_threshold


	def update(self):

		count = 0


		for cell in self.cell_membrane.cells:
		

			trigger_signal_anti = cell.anticlockwise_neighbour.state
			trigger_signal_clock = cell.clockwise_neighbour.state
			self_strain = cell.get_strain()[0] 
	
			
			if (trigger_signal_anti == 1 or trigger_signal_clock == 1) and self_strain <self.upper_threshold:
				cell.state = 1
				cell.growing = True
			elif self_strain > self.upper_threshold:
				cell.growing = False
				cell.state = 0

			if cell.growing:
				cell.state = 1


			count += 1
		




