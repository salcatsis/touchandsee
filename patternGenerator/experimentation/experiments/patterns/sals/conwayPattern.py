from pattern import Pattern
import numpy as np 
import time

class ConwayPattern(Pattern):

	def __init__(self,membrane,lower_threshold,upper_threshold):

		self.cell_membrane  = membrane
		self.lower_threshold = lower_threshold
		self.upper_threshold = upper_threshold


	def update(self):

		count = 0

		for cell in self.cell_membrane.cells:


			trigger_strain_anti = cell.anticlockwise_neighbour.get_strain()[0]
			trigger_strain_clock = cell.clockwise_neighbour.get_strain()[0]

			trigger_strain = np.mean((trigger_strain_anti,trigger_strain_clock))
			self_strain = cell.get_strain()[0]


			if trigger_strain > self.upper_threshold and self_strain <= self.lower_threshold + 0.01: # and self_strain < self.lower_threshold:  
				cell.state = 1
				cell.growing = True

			if trigger_strain < self.upper_threshold and not cell.growing:
				cell.state = 0
			elif trigger_strain < self.upper_threshold and cell.growing:
				cell.state = 1

			if self_strain >= (self.upper_threshold*2):
				cell.state = 0
				cell.growing = False




			count += 1
		




