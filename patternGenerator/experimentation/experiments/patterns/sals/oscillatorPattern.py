from pattern import Pattern
import time

class OscillatorPattern(Pattern):

	def __init__(self,membrane,lower_threshold,upper_threshold ):

		self.cell_membrane  = membrane
		self.lower_threshold = lower_threshold
		self.upper_threshold = upper_threshold


	def update(self):

		for cell in self.cell_membrane.cells:

			trigger_strain = cell.get_strain()[0]

			if trigger_strain < self.lower_threshold:
				cell.state = 1
			elif trigger_strain > self.upper_threshold:
				cell.state = 0



