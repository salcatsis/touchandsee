from pattern import Pattern
import time
import patternGenerator.experimentation.experiments.plotUtils as plotUtils
import numpy as np

class DifferencePattern(Pattern):
    """
        Cell ahead - Cell in front
    """
    def __init__(self, membrane, 
        on_threshold=0.025, 
        off_threshold=-0.03,
        hold_angle=60,
        hold_time=10,
        hold_start=10
    ):
        Pattern.__init__(self, membrane,
            on_threshold=on_threshold,
            off_threshold=off_threshold,
            hold_angle=hold_angle,
            hold_time=hold_time,
            hold_start=hold_start
        )

        self.switched=False
        self.held = False

    def update(self, t):
        for cell in self.membrane.cells:
            source = DifferencePattern.source(cell)

            # Switching state
            if cell.state and source < self.off_threshold:
                cell.state = not cell.state
            elif not cell.state and source > self.on_threshold:
                cell.state = not cell.state

        if not self.switched:
            self.switched=True
            return "default"

        # if t>=self.hold_start and not self.held:
        #     self.held=True
        #     return ("h", self.hold_angle, self.hold_time)



    @staticmethod
    def source(cell, weight=1):
        return cell.previous_cell.stretch_ratio() - cell.next_cell.stretch_ratio()


    @staticmethod
    def plot(trial):
        #source_plot(trial)
        num_plot(trial)

def num_plot(trial):
    fig, ax = plotUtils.new_axes()

    times = trial.timestamps()
    num_cells_on=[]
    for t in times:
        num_cells_on.append(np.sum([cell.state for cell in trial.samples[t].membrane.cells]))

    num_cells_on = np.transpose(num_cells_on)
    ax.plot(times, num_cells_on)
    ax.set_xlabel("Time (s)")
    ax.set_ylabel("Number of Activated Cells")
    ax.grid()

def source_plot(trial):
    fig, ax = plotUtils.new_axes()

    times = trial.timestamps()
    sources=[]
    for t in times:
        sources.append([DifferencePattern.source(cell) for cell in trial.samples[t].membrane.cells])

    sources = np.transpose(sources)

    for i, source in enumerate(sources):
        ax.plot(times, source, label="Cell %i"%(i+1))

    thresh_color='k'
    thresh_thickness=1

    ax.axhline(y=trial.pattern_info["params"]["on_threshold"], linewidth=thresh_thickness, color=thresh_color, linestyle="--")
    ax.axhline(y=trial.pattern_info["params"]["off_threshold"], linewidth=thresh_thickness, color=thresh_color, linestyle="--")


    ax.legend(loc="upper right")
    ax.grid()
    ax.set_xlabel("Time (s)")
    ax.set_ylabel("Source")
    ax.set_title("Source Plot")

