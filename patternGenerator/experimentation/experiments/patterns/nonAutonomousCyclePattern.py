from pattern import Pattern
import time
import itertools


class NonAutonomousCyclePattern(Pattern):
    """
        Turns one cell on at a time with a constant period
    """
    
    def __init__(self, membrane, period=3):
        Pattern.__init__(self, membrane, {
            "period": period
        })

        self.next_switch = time.time() + self.period
        self.cycle = itertools.cycle(range(len(self.cell_membrane.cells)))

    def update(self):
        """
            Check to see if we are ready to switch to the next cell
        """
        if time.time() > self.next_switch:
            self.next_switch += self.period

            # Turn all cells off
            for cell in self.cell_membrane.cells:
                cell.state = False

            # Turn the next one
            self.cell_membrane.cells[next(self.cycle)].state = True
