from pattern import Pattern
import time
import matplotlib.pyplot as plt

"""
    Just motor in the beginning, nothing else
"""

class MotorPattern(Pattern):
    def __init__(self, membrane, sweep_angle=180):
        Pattern.__init__(self, membrane, 
            sweep_angle = sweep_angle
        )

        self.swept = False

    def update(self, t):
        if not self.swept:
            self.swept = True
            return ["default", "default"]

    @staticmethod
    def plot(trial):
        pass


