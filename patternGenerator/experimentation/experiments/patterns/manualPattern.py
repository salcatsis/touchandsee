from pattern import Pattern

import threading

class ManualPattern(Pattern, threading.Thread):
    """
        Manually toggle switches
    """
    def __init__(self, membrane):
        Pattern.__init__(self, membrane)

        # Initialise Thread
        threading.Thread.__init__(self)
        self.daemon=True

        self.lock = threading.Lock()
        self.start()

    def update(self):
        pass

    def run(self):
        """
            Thread to get input from user.
            TODO: This doesn't work with pattern gen because of the carriage returns pg generates
        """
        # Get input fro user
        states_string = raw_input("Enter in cell states: ")

        # Convert to boolean
        success = True
        states = []
        for c in states_string:
            if c=="1" or c=="0":
                states.append(bool(int(c)))
            else:
                success = False
                break

        # Update cells
        if success:
            with self.lock:
                for i in range(min([len(states), len(self.cell_membrane.cells)])):
                    self.cell_membrane.cells[i].state = states[i]
        else:
            print "Bad input, should be a list of 1's (on) and 0's (off)"


