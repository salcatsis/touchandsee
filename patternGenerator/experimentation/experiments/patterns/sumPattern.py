from pattern import Pattern
import time
import patternGenerator.experimentation.experiments.plotUtils as plotUtils
import numpy as np

class SumPattern(Pattern):
    """
        Cell ahead - Cell in front
    """
    def __init__(self, membrane, 
        on_threshold=2.04, 
        off_threshold=2.02,
    ):
        Pattern.__init__(self, membrane,
            on_threshold=on_threshold,
            off_threshold=off_threshold
        )

        self.switched=False

    def update(self, t):
        for cell in self.membrane.cells:
            source = SumPattern.source(cell)

            # Switching state
            if cell.state and source < self.off_threshold:
                cell.state = not cell.state
            elif not cell.state and source > self.on_threshold:
                cell.state = not cell.state

        if not self.switched:
            self.switched=True
            return "default"

    @staticmethod
    def source(cell, weight=1):
        return cell.previous_cell.stretch_ratio() + cell.next_cell.stretch_ratio()


    @staticmethod
    def plot(trial):
        source_plot(trial)
        num_plot(trial)

def num_plot(trial):
    fig, ax = plotUtils.new_axes()

    times = trial.timestamps()
    num_cells_on=[]
    for t in times:
        num_cells_on.append(np.sum([cell.state for cell in trial.samples[t].membrane.cells]))

    num_cells_on = np.transpose(num_cells_on)
    ax.plot(times, num_cells_on)
    ax.set_xlabel("Time (s)")
    ax.set_ylabel("Num Cells on")
    ax.set_title("Number of cells on")


def source_plot(trial):
    fig, ax = plotUtils.new_axes()

    times = trial.timestamps()
    sources=[]
    for t in times:
        sources.append([DifferencePattern.source(cell) for cell in trial.samples[t].membrane.cells])

    sources = np.transpose(sources)

    for i, source in enumerate(sources):
        ax.plot(times, source, label="Cell %i"%(i+1))

    thresh_color='k'
    thresh_thickness=1

    ax.axhline(y=trial.pattern_info["params"]["on_threshold"], linewidth=thresh_thickness, color=thresh_color, linestyle="--")
    ax.axhline(y=trial.pattern_info["params"]["off_threshold"], linewidth=thresh_thickness, color=thresh_color, linestyle="--")


    ax.legend(loc="upper right")
    ax.grid()
    ax.set_xlabel("Time (s)")
    ax.set_ylabel("Source")
    ax.set_title("Source Plot")

