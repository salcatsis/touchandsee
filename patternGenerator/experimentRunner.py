"""
    A convenitent experiment launcher
    TODO: Find a better place for me!
"""
import argparse

from experimentation.experiments import *
from experimentation.experiments.patterns import *
import experimentation.experiments.differenceExperiment
from experimentation.experiments.patterns.pattern import Pattern
from experimentation.experiments.patterns.twoCellPattern import TwoCellPattern
import experimentation.experiments.plotUtils as plotUtils
import experimentation.experiments.patterns.patternList as patternList

import matplotlib.pyplot as plt

from generator import Generator

from experimentation.trial import load_from_video
from experimentation.trial import save_to_video

import numpy as np

import matplotlib
matplotlib.rc('font', **{
    'weight' : 'bold',
    'size'   : 12
    }
)

def main(args):
    parser = argparse.ArgumentParser(description="Run an experiment")
    parser.add_argument("mode", help="Run or Analyse", choices=[
        "run",
        "analyse"
    ])

    parser.add_argument("experiment", help="Name of experiment")
    parser.add_argument("directory", help="Directory to save/load")

    args = parser.parse_args(args)

    # Form experiment dictionary
    experiment_dict = {}
    for experiment in Experiment.__subclasses__():
        experiment_dict[experiment.__name__] = experiment

    # Match experiment
    if args.experiment not in experiment_dict:
        print "UNKNOWN EXPERIMENT", args.experiment
        return

    experiment = experiment_dict[args.experiment](args.directory)

    # Perform task
    if args.mode == "run":
        experiment.start(Generator())

    elif args.mode == "analyse":
        experiment.analyse()

def pattern_runner(args):
    """
        Run a singular pattern
    """

    # Arg parse
    parser = argparse.ArgumentParser(description="Run an experiment")
    parser.add_argument("pattern", help="Name of pattern")
    parser.add_argument("filename", help="Where to save file (must be .mp4)")
    parser.add_argument("-t", type=int, default=1, help="How many seconds to run for")

    args = parser.parse_args(args)

    # Choose a pattern
    pattern_dict = {}
    for pattern in Pattern.__subclasses__():
        pattern_dict[pattern.__name__] = pattern

    if args.pattern not in pattern_dict:
        print "UNKNOWN PATTERN: ", args.pattern
        print "AVAILABLE", pattern_dict.keys()
        return

    pattern = pattern_dict[args.pattern]

    # Generate!
    generator = Generator()

    trial = generator.run(pattern(generator.membrane()), filename=args.filename, pattern_time=args.t)

    save_to_video(args.filename, trial)


def plotter(args):
    parser = argparse.ArgumentParser(description = "Plot a Trial")
    parser.add_argument("path", help="Path to trial")

    args = parser.parse_args(args)

    trial = load_from_video(args.path)

    plotUtils.state_plot(trial)
    plotUtils.diameter_plot(trial)
    plotUtils.diameter_plot_lines(trial)

    # HACK: This is why I should migrate my data
    if np.any(trial.membrane.pull.position):
        plotUtils.pull_plot(trial)

    # Do any pattern specific plotting
    pattern_name = trial.pattern_info["name"]
    if pattern_name in patternList.patternDict:
        pattern = patternList.patternDict[pattern_name]
        
        if "plot" in dir(pattern):
            pattern.plot(trial)

    plt.show()


