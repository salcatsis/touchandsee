"""
    Represents the location of the pull point
"""
import numpy as np

class Pull:
    def __init__(self, x=None, y=None):
        self.initial_position = np.array([x,y])
        self.position = np.array([x,y])

    def set_position(self, x, y):
        self.position = np.array([x,y])

    def copy(self):
        copied = Pull(*self.initial_position)
        copied.set_position(*self.position)

        return copied