import json
import os

"""
    Dimensions of the skin        
"""

default_dimensions_path = os.path.dirname(__file__) + "/" + "default_dimensions.json"


# Load Dimensions
def load(filepath=default_dimensions_path):
    # Simple deserialise
    with open(filepath, 'r') as f:
        return json.load(f)


# Save settings
def save(dimensions, filepath=default_dimensions_path):
    # JSON Dump
    with open(filepath, 'w') as f:
        f.write(json.dumps(dimensions, indent=4))
