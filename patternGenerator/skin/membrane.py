import cv2
import numpy as np
import math
from sklearn.neighbors import NearestNeighbors as nn
from scipy.spatial import KDTree as kdt

import matplotlib.pyplot as plt
from cell import Cell

from pull import Pull

class Membrane:
    """
        A piece of artificial skin with cells and a puller
    """
    def __init__(self, cells, dimensions, pull=None):
        # Find centroid of cells
        centroid = np.mean([cell.initial_centroid for cell in cells], 0) if cells else np.array([0,0])

        # Order cells counter clockwise
        self.cells = sorted(cells, key=lambda x: np.arctan2(*(x.initial_centroid-centroid)))

        # Clockwise assignment
        # TODO: multiple cell arrangements
        for i, cell in enumerate(self.cells):
            cell.next_cell = self.cells[(i + 1) % len(cells)]
            cell.previous_cell = self.cells[(i - 1) % len(cells)]

        # Cell Dimensions
        self.dimensions = dimensions

        # The pull point
        if not pull:
            pull = Pull()
        self.pull = pull

    # TODO: Cell Linking
    def copy(self):
        return Membrane([cell.copy() for cell in self.cells], self.dimensions, self.pull.copy())


# Simple skin plotting
# TODO: Plot the pull point
def plot_membrane(membrane):
    # Add cells to plot
    for cell in membrane.cells:
        # Plot Cells
        circle = plt.Circle(cell.centroid, radius=cell.diameter / 2, color='k')
        plt.gca().add_artist(circle)

        # Cell coupling
        plt.gca().annotate("", xy=cell.centroid, xytext=cell.next_cell.centroid, arrowprops={
            "arrowstyle": "->",
            "color": "blue"
        })

        plt.gca().annotate("", xy=cell.centroid, xytext=cell.previous_cell.centroid, arrowprops={
            "arrowstyle": "->",
            "color": "blue",
            "lw": 2,
        })

    # Histogram
    pts = np.abs(np.ravel([cell.centroid for cell in membrane.cells]))
    lim = 1.5 * np.max(pts)
    plt.xlim(-lim, lim)
    plt.ylim(-lim, lim)

    plt.grid()
    plt.show()


if __name__ == '__main__':
    plot_membrane(Membrane([
        Cell([2, 0], 1, state=1),
        Cell([0, 2], 1, state=1),
        Cell([0, -2], 1, state=1),
        Cell([-2, 0], 1, state=1)
    ]))
