import numpy as np


## Classes
# TODO: Numpyise centroids
class Cell:
    def __init__(self, initial_centroid, initial_diameter, state=0):
        self.initial_diameter = float(initial_diameter)
        self.diameter = float(initial_diameter)

        self.initial_centroid = np.array(initial_centroid)
        self.centroid = np.array(initial_centroid)

        self.state = state

        # Should be cells
        self.next_cell = None
        self.previous_cell = None

    # TODO: Currently copies without any linking
    def copy(self):
        new_cell = Cell(self.initial_centroid, self.initial_diameter, self.state)
        new_cell.update(self.centroid, self.diameter)

        return new_cell

    def area(self):
        return np.pi * self.diameter

    def stretch_ratio(self):
        return self.diameter / self.initial_diameter

    def update(self, centroid, diameter):
        self.centroid = np.array(centroid)
        self.diameter = float(diameter)
