import cv2
import numpy as np

def image_center(frame):
    """
        Return the coordinates of the center of an image
        :param frame: np image array
        :return: tuple
    """

    return tuple(np.array(np.float64(frame.shape[0:2]) / 2.0, dtype=np.int))


def resize(frame, height_pixels):
    """
        Resize an image maintianing sapect ratio to a specified image height
        :param frame:
        :param height_pixels:
        :return:
    """
    height, width, _ = frame.shape
    aspect_ratio = float(width) / height

    width_pixels = int(aspect_ratio * height_pixels)
    return cv2.resize(frame, (width_pixels, height_pixels))


def clamp(x, minn, maxx):
    """
        Limit the range of x between the given limits
    :param x:
    :param minn:
    :param maxx:
    :return:
    """
    clamped = x

    if x < minn:
        clamped = minn
    elif x > maxx:
        clamped = maxx

    return clamped


def contour_centroid(contour):
    """
        Centroid of a contour using moments
    :param contour: a cv contour
    :return: coordinates of centroid
    """
    M = cv2.moments(contour)

    centroid_x = int(M["m10"] / M["m00"])
    centroid_y = int(M["m01"] / M["m00"])

    return centroid_x, centroid_y


def circle_diameter(area):
    """
        Diameter of a circle
    :param area: area of circle
    :return: diameter of circle
    """
    diameter = np.sqrt(4 * area / np.pi)
    return diameter
