import skvideo.io
import skvideo.datasets


import time
import sys
import threading
import numpy as np

from cameraUtils import live_stream
from camera import Camera

class VideoStreamer(Camera):
    """ Streams a video file from the hard disk, can be used to mock PointGreyCamera"""
    def __init__(self, video_file):
        self.video_file = video_file

        # Get video metadata
        frames = skvideo.io.vreader(self.video_file)
        metadata = skvideo.io.ffprobe(self.video_file)["video"]

        self.pixel_width = int(metadata["@width"])
        self.pixel_height = int(metadata["@height"])

        self.frame_rate = int(round(float(metadata["@nb_frames"]) / float(metadata["@duration"])))

        # Setup acquisition thread
        self.thread = threading.Thread(target=self.run)
        self.thread.daemon = True
        self.lock = threading.Lock()

        # Initialise output frame
        self.frame = np.zeros((self.pixel_width, self.pixel_height, 3), dtype='uint8')

    # Filename is not needed here since we are already streaming from a video
    def start(self, filename=None):
        self.thread.start()

    # Takes frames at the same rate as the video file framerate
    def run(self):
        # Loop the video forever-and-ever-and-ever
        while True:
            videogen = skvideo.io.vreader(self.video_file)

            period = 1.0/self.frame_rate
            next_frame = time.time()
            for frame in videogen:
                next_frame += period

                # Thread safe copy of the frame
                with self.lock:
                    self.frame = frame.copy()

                # Sleep for the remainder of the period
                time.sleep(max([0, next_frame - time.time()]))

    def get_frame(self):
        # Get the pointer to the copy of the frame safely
        with self.lock:
            frame = self.frame

        return frame

    # TODO
    def stop(self):
        pass


if __name__ == '__main__':
    # Get filename
    if len(sys.argv) < 2:
        print "Supply video filename as first arg"
        quit()

    filename = sys.argv[1]

    # Stream it up
    video_streamer = VideoStreamer(filename)
    print "Streaming", filename
    live_stream(video_streamer, "", 100)