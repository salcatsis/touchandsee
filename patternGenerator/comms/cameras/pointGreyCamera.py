## Imports
from __future__ import division

# Depends on FlyCapture2 SDK
try:
	import flycapture2 as fc2
	fly_capture_installed = True
except ImportError:
	fly_capture_installed = False

import skvideo.io
from scipy.misc import imsave
from matplotlib import pyplot as plt
import numpy as np
import cv2

import time
import argparse
import threading
import os

from cameraUtils import live_stream
from camera import Camera


class PointGreyCamera(Camera):
	# Define class variables for the camera
	# Throw an exception it cannot connect to camera
	def __init__(self, pixel_width=1024, pixel_height=1024, frame_rate=30):
		if not fly_capture_installed:
			raise ImportError("Fly capture 2 is not installed")

		# Image size definitions
		self.pixel_height= pixel_height
		self.pixel_width = pixel_width
		self.cam  = fc2.Context()

		# Gain control of a PG camera
		try:
			self.cam.connect(*self.cam.get_camera_from_index(0))
			print 'Camera connected...'
		except:
			raise Exception('Cannot connect to point grey camera...')

		# Format7 optimises framerate with respect the 7 config parameters passed here
		# It is by far the most user friendly way to setup the camera
		self.cam.set_format7_configuration(2,0,0,pixel_width,pixel_height,134217728)
		self.is_running = False
		self.thread = None
		self.frame_lock = threading.Lock()
		
		# TODO: Tihs framerate is somewhat artificial since we don't know what Format7 has decided
		# But the Grasshopper3 we've been using can to 80fps, well above what we need for artificial chromatophores
		self.frame_rate = frame_rate

		# Initialise frame to black
		self.cv_frame = np.zeros((self.pixel_width,self.pixel_height,3), dtype = 'uint8')

	# The thread which acquires frames from the camera and writes them to a video object
	# Start the camera and create a new thread on which to acquire frames from the camera
	def start(self, filename=None):
		# Just return straight away if it is already running
		if self.is_running:
			return

		# Start frame acuqisition
		self.cam.start_capture()
		
		# Set flags#
		# TODO use inbuilt features for flags
		with self.frame_lock:
			self.stop_acquisition = False
			self.is_running = True

		# Start the thread
		self.thread = threading.Thread(target = self.acquisition_thread, args = (filename,))
		self.thread.daemon = True
		self.thread.start()

		# Wait for the first frame to acutally be acuired
		# TODO: Use inbuilt flags?
		while np.sum(self.get_frame()) == 0:
			pass
	
	def acquisition_thread(self,filename):
		# Setup video writing
		if filename:
			writer = skvideo.io.FFmpegWriter(filename, outputdict = {
				'-vcodec': 'libx264rgb',
				'-preset': 'ultrafast',
				"-framerate":str(self.frame_rate),
				"-pix_fmt": "rgb24"
			}, inputdict={
				"-framerate": str(self.frame_rate)
			})
		
		# Main Acuisition cycle loop
		acquisition_period = 1.0/self.frame_rate
		next_acquisition = time.time() + acquisition_period
		while not self.stop_acquisition:
			
			# Acquire a Frame
			frame = fc2.Image()			
			self.cam.retrieve_buffer(frame)

			# TODO: This are 3 copies now (incl one in get frame), shouldn't need so many!
			cv_frame = np.array(frame).copy()
			cv_frame2 = cv_frame.copy()
	
			# Perform a safe copy
			with self.frame_lock:
				self.cv_frame = cv_frame2

			# Write to file
			if filename:
				writer.writeFrame(cv_frame)

			# Wait for the next acquisition cycle (more accurate than time.sleep)
			# TODO: Check that we have looped at least once (i.e. framerate isn't set too high)
			#       Important to make sure the output video time is faithful
			while time.time() < next_acquisition:
				pass

			next_acquisition += acquisition_period

		# Tidy up
		self.cam.stop_capture()
		if filename:
			writer.close()

	# Stop the acquisition thread by setting the flag and then stop the camera
	def stop(self):
		with self.frame_lock:
			is_running = self.is_running

		if is_running:
			with self.frame_lock:
				self.stop_acquisition = True
				self.is_running = False

			self.thread.join()

	# Safely obtain the copy of the frame from the acquisition thread
	def get_frame(self):
		with self.frame_lock:
			cv_frame = self.cv_frame

		return cv_frame.copy()


def main(args):
	# Construct the argument parse and parse the arguments
	argument_parse = argparse.ArgumentParser()

	argument_parse.add_argument("-f","--filename", help = "input filename for video", default=None)
	argument_parse.add_argument("-s", "--stream", action='store_true')
	argument_parse.add_argument("-l", "--videolength", help = "length of video, 0 means run forever", default=0, type=int)
	argument_parse.add_argument("-i", "--image", help="Take a snapshot")

	args = argument_parse.parse_args(args)

	# Create camera object
	cam = PointGreyCamera()

	# A live stream
	if args.stream:
		live_stream(cam, args.filename, args.videolength)
		cam.stop()

	# Record some video
	elif args.videolength:
		cam.start(args.filename)
		if args.videolength:
			time.sleep(args.videolength)
			cam.stop()
			print 'video capture completed'
		else:
			print 'Running forever...'

	# Save an image to the hard drive
	elif args.image:
		cam.start(None)
		frame = cam.get_frame()
		cam.stop()
		imsave(args.image, frame)
		
## Script
if __name__ == "__main__":
    main(sys.argsv[1:])