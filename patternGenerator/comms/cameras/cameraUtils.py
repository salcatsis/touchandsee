import cv2
import time

# A heads up display for video that is streaming 
def live_stream(camera, filename=None, video_length=float("inf"), height_pixels=400):
	# Determine Frame Size
	aspect_ratio = float(camera.pixel_width)/camera.pixel_height
	width_pixels = int(aspect_ratio * height_pixels)

	# Start the camera
	camera.start(filename)
	start_time = time.time()

	if video_length <=0:
		video_length = float("inf")

	# Display frames
	while time.time()-start_time < video_length:
		# Acuire frames and resize
		cv_frame = camera.get_frame()
		cv_frame = cv2.cvtColor(cv_frame,cv2.COLOR_RGB2BGR)
		frame_show = cv2.resize(cv_frame, (width_pixels, height_pixels))

		# Display to user
		cv2.imshow('output', frame_show)
		if cv2.waitKey(int(1000.0/camera.frame_rate)) == ord('q'):
			break

	camera.stop()
