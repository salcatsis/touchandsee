import threading
import socket
import time
import sys

import patternGenerator.settings as default_settings

class VoltageServer(threading.Thread):
    '''
        Listen for voltage data over UDP
    '''
    def __init__(self, port=5500, ip="192.168.0.101"):
        # Initialise comms thread
        threading.Thread.__init__(self)
        self.daemon = True
        
        self.port = port
        self.ip = ip

        # Voltage Reading
        self.voltage = 0

        self.lock = threading.Lock()

        self.start()

    # TODO: Stop flag
    def run(self):
        '''
            server thread
        '''
        sock = socket.socket(socket.AF_INET, # Internet
                             socket.SOCK_DGRAM) # UDP
        
        sock.bind((self.ip, self.port))
        
        while True:
            data, addr = sock.recvfrom(1024) # buffer size is 1024 bytes
            
            with self.lock:
                self.voltage = float(data)

    def get_voltage(self):
        '''
            Thread safe get voltage
        '''
        with self.lock:
            return self.voltage

def main(args):
    settings = default_settings.load()
    ratio = settings["pattern_generator"]["voltage_divider_ratio"]

    # An example of how to use the votlage server
    server = VoltageServer(
        port=settings["voltage_server"]["port"],
        ip=settings["voltage_server"]["ip"]
    )

    while True:
        voltage = server.get_voltage()
        print "\r%.1fV" % voltage,
        print "(corrected: %.0fV)" % (voltage/ratio),
        sys.stdout.flush()


if __name__ == '__main__':
    main(sys.argv[1:])

