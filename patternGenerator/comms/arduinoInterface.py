# Imports

import serial
import time
import argparse
import random
# Global variables

# Classes
import time
import itertools
import re
import threading
import os

# TODO: framerate should really be enforced in arduino 
framerate = 20

class ArduinoInterface(threading.Thread):
	"""
		Handles communication with an arduino controller
	"""
	def __init__(self, port=None,  baud=115200, acknowledge_message="1"):
		self.acknowledge_message = acknowledge_message

		if port is None:
			if os.path.exists('/dev/ttyACM0'):
				port = '/dev/ttyACM0'
			elif os.path.exists('/dev/ttyACM1'):
				port = '/dev/ttyACM1'
			else:
				raise Exception("Cannot find Arduino")

		try:
			self.arduino =serial.Serial(port,baud)
			#self.wait_for_arduino()
		except:
			message = 'port =' + str(port) +' baud = ' + str(baud)
			raise Exception('Could not connect to arduino ... ' + message)

		# Message types
		self.cell_states = None
		self.sweep_params = None
		self.hold_params = None
		self.sending = False

		# TODO: arduino should really send a ready message
		time.sleep(2)

		# Initialise comms thread
		threading.Thread.__init__(self)
		self.daemon = True

		self.lock = threading.Lock()
		self.start()

		

	def run(self):
		"""
			Endlessly waits for messages from arduino
		"""

		# If you switch the relays too quickly, arduino crashes. 
		# TODO: This should really implemented arduino side 
		next_switch_time = 0

		while True:
			if self.sending:
				# Read from the arduino
				line = self.arduino.readline()

				# Check for cell states acknowledgement
				if line[0] == self.acknowledge_message:
					with self.lock:
						self.sending = False

				# Update next allowable send
				next_switch_time = time.time() + 1.0/framerate

			# Sweepington
			elif self.sweep_params is not None:
				self.sending = True

				with self.lock:
					sweep_angle, sweep_time = self.sweep_params
					self.sweep_params = None

				self.arduino.write("s%i:%f\n"%(sweep_angle,sweep_time))

			elif self.hold_params is not None:
				self.sending = True

				with self.lock:
					hold_angle, hold_time = self.hold_params
					self.hold_params = None

				self.arduino.write("h%i:%f\n"%(hold_angle,hold_time))

			# Send cell states to arduino
			elif self.cell_states and time.time() > next_switch_time:
				with self.lock:
					states = self.cell_states
					self.cell_states = None

				self.sending = True
				self.arduino.write(''.join([str(int(x)) for x in states])+'\n')

	def hold(self, hold_angle, hold_time):
		with self.lock:
			self.hold_params = (hold_angle, hold_time)

	# TODO: Use design pattern as send states
	def sweep(self, sweep_angle, sweep_time):
		with self.lock:
			self.sweep_params = (sweep_angle, sweep_time)		

	def send_states(self, states):
		"""
			Sends binary states to arduino
			states should be a binary array of ints or bools
		"""
		#TODO: Check format of states
		with self.lock:
			self.cell_states = states

	# TODO - time out if this doesnt work...
	def wait_for_arduino(self, timeout=5):
		"""
			Hangs until a confirmation message is received from the arduino
		"""

		timeout_time = time.time() + timeout

		msg=""
		while msg != self.acknowledge_message:
			msg = self.arduino.readline().strip()

			if time.time() > timeout_time:
				raise Exception("No acknowledgemnt from arduino")


# Send random data to arduino
if __name__ == '__main__':
	arduino = ArduinoInterface()

	t_start = time.time()

	while True:

		states = [random.random() < 0.5 for i in range(5)]
		arduino.send_states(states)

		print "Elasped", time.time() - t_start