## Imports

import skvideo.io
import numpy as np
import cv2
import os
import argparse
import matplotlib.pyplot as plt

from patternGenerator.skin.cell import Cell
from patternGenerator.skin.membrane import Membrane
from patternGenerator.skin.pull import Pull
import patternGenerator.skin.dimensions as dimensions

from patternGenerator.comms.cameras.pointGreyCamera import PointGreyCamera
import patternGenerator.settings as settings

from patternGenerator.comms.voltageServer import VoltageServer

import patternGenerator.settings as default_settings

from matplotlib import pyplot as plt
from heapq import nlargest
from scipy.spatial import KDTree as kdt
import scipy.misc

import patternGenerator.utils as utils
import sys

import time
import math
from sklearn.neighbors import NearestNeighbors as nn


# TODO : Checking on frame dimensions
class Tracker:
    # Calibrate on creation of object
    # If video is passed for debugging then calibrate on the first frame of the video
    def __init__(self, frame, 
        dimensions, 
        crop_radius=600, 
        kernel_size=5, 
        center=None,
        binary_threshold=90
    ):
        self.dimensions = dimensions

        self.crop_radius = crop_radius

        self.binary_threshold = binary_threshold

        # A calibration frame for reference
        # TODO: Make calibrate use this
        self.calibration_frame = frame

        # Set center of crop
        self.center = center if center else utils.image_center(frame)
        self.set_center(self.center)

        # Blur Level
        self.kernel_size = kernel_size

        self.membrane = self.calibrate(dimensions)

    def number_of_cells(self):
        return self.dimensions["num_cells"]

    def set_center(self, center):
        """
            Sets the center of the crop circle
            the range of the center is automatically forced to be within the calibration frame
        """
        if len(center) != 2:
            raise Exception("Need two ints to define center on tracker")

        x = int(utils.clamp(center[0], 0, self.calibration_frame.shape[0]))
        y = int(utils.clamp(center[1], 0, self.calibration_frame.shape[1]))

        self.center = (x, y)

    def crop(self, frame):
        """
            A circular crop used for frame processing
        """

        # Useful things for masking
        # A filled circle for a mask
        mask = np.zeros([frame.shape[0], frame.shape[1]], dtype="uint8")
        mask = cv2.circle(mask, self.center, self.crop_radius, 255, -1)
        mask_inverse = cv2.bitwise_not(mask)

        # A white base image to add the cropped image onto
        white = 255 * np.ones(frame.shape, dtype="uint8")

        # Mask both images
        frame_mask = cv2.bitwise_and(frame, frame, mask=mask)
        white_mask = cv2.bitwise_and(white, white, mask=mask_inverse)

        # Add the result
        return cv2.add(frame_mask, white_mask)

    # TODO: Calibrate the camera to determine how many cells there are and find the diameters of the contours
    # At the moment I have set the number of expected cells as user defined variable to make the tracking easier...
    def calibrate(self, dimensions):
        contours = self.get_contour(self.calibration_frame)

        # Choose the largest ones as the cells, and the next largest as the pull spot
        cell_contours = contours[:-1] if self.dimensions["pull_spot"] else contours

        # Construct the cells
        cells = []
        for contour in cell_contours:
            centroid = utils.contour_centroid(contour)
            area = cv2.contourArea(contour)
            diameter = utils.circle_diameter(area)

            cells.append(Cell(centroid, diameter))

        # Make the pull spot
        if self.dimensions["pull_spot"]:
            pull = Pull(*utils.contour_centroid(contours[-1]))
            return Membrane(cells, dimensions, pull=pull)
        else:
            return Membrane(cells, dimensions)

    def threshold(self, frame):
        # TODO - deal with failure cases...
        # Read in frame to process

        unprocessed_frame = frame

        # Convert to correct format for opencv
        coloured_frame = cv2.cvtColor(unprocessed_frame, cv2.COLOR_RGB2BGR)

        # Circular Crop
        crop_frame = self.crop(coloured_frame)

        # Grey scale the frame
        grey = cv2.cvtColor(crop_frame, cv2.COLOR_BGR2GRAY)

        # Blur the frame
        blurred = cv2.GaussianBlur(grey, (self.kernel_size, self.kernel_size), 0)

        #Other deprecated binaryization
        # processed_frame = cv2.adaptiveThreshold(blurred,255,cv2.ADAPTIVE_THRESH_MEAN_C,\
        #    cv2.THRESH_BINARY,11,2)


        # Otsu's thresholding after Gaussian filtering
        # processed_frame = cv2.adaptiveThreshold(blurred,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,\
        #    cv2.THRESH_BINARY,11,2)


        ret, processed_frame = cv2.threshold(blurred, self.binary_threshold, 255, cv2.THRESH_BINARY)

        return processed_frame

    # Find the cell diameter of all the cells using a contouring algorithm
    def get_contour(self, frame):
        thresholded = self.threshold(frame)

        # Find the contours of the processed frame
        _, contours, hierarchy = cv2.findContours(thresholded, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

        # Find the area of those contours
        areas = [cv2.contourArea(c) for c in contours]

        # Account for tracking spot
        num_contours = self.number_of_cells() + 2 if self.dimensions["pull_spot"] else self.number_of_cells() + 1

        # Sort the areas and draw the second largest contour which is assumed to be the cell contour (n largest contours for n cells)
        sorted_areas = nlargest(num_contours, range(0, len(areas)), key=lambda i: areas[i])

        return [contours[x] for x in sorted_areas[1:num_contours]]

    # TODO: Pull spot
    def process(self, frame):
        # Get the contours
        contours = self.get_contour(frame)

        # Choose the largest ones as the cells, and the next largest as the pull spot
        cell_contours = contours[:-1] if self.dimensions["pull_spot"] else contours

        # Assign the cells
        original_centroids = [cell.initial_centroid for cell in self.membrane.cells]
        original_tree = kdt(original_centroids)

        for contour in cell_contours:
            centroid = utils.contour_centroid(contour)
            area = np.sqrt(4 * cv2.contourArea(contour) / np.pi)

            index = original_tree.query(centroid)[1]
            self.membrane.cells[index].update(centroid, area)

        # Assign the spot
        if self.dimensions["pull_spot"]:
            self.membrane.pull.set_position(*utils.contour_centroid(contours[-1]))

        return contours


def render_video(tracker, infile, outfile, plot=False):
    video = skvideo.io.vreader(infile)

    # Get framerate
    # TODO: DRY This up
    metadata = skvideo.io.ffprobe(infile)["video"]
    framerate = int(float(metadata["@nb_frames"]) / float(metadata["@duration"]))

    writer = skvideo.io.FFmpegWriter(outfile,
        outputdict={
            '-vcodec': 'libx264rgb',
            '-preset': 'ultrafast',
            '-framerate': str(framerate),
            '-pix_fmt': 'rgb24'
        }, inputdict={
            '-framerate': str(framerate)
        })

    count = 0

    stretches = []

    for frame in video:
        count += 1
        print "\rFrame", count, "of", metadata["@nb_frames"],
        sys.stdout.flush()

        contours = tracker.process(frame)

        # HACK! Migrate ya data you lazy basterd#
        if len(contours) > tracker.number_of_cells():
            cv2.drawContours(frame, [contours[-1]], -1, (255, 0, 0), 3)
            cv2.circle(frame, tuple(tracker.membrane.pull.position.tolist()), 5, (0,0,255), -1)
            del contours[-1]

        cv2.drawContours(frame, contours, -1, (0, 255, 0), 3)

        stretches.append([cell.stretch_ratio() for cell in tracker.membrane.cells])

        writer.writeFrame(frame)

    writer.close()
    stretches = np.array(stretches)

    if plot:
        t = np.linspace(0, len(stretches)/float(framerate), len(stretches))

        for stretch in np.transpose(stretches):
            plt.plot(t, stretch)
            plt.xlabel("Time (s)")
            plt.ylabel("Stretch Ratio")

        plt.grid()
        plt.show()

    


def plot_image(tracker, image):
    plt.imshow(image)
    image = cv2.drawContours(image, tracker.get_contour(image), -1, (0, 255, 0), 3)

    plt.imshow(image)
    plt.show()

 