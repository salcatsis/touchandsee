import cv2
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure

import threading
from . import Tracker
from . import PointGreyCamera
from . import utils
from . import settings
from . import dimensions

import argparse
import re
import time
import json

# TODO Unit Tests
class Calibrator(threading.Thread):
    """
        A tool to calibrate the tracker with a head up display
    """
    def __init__(self, tracker_settings, num_cells=0, display_image_height=None):#
        # Default value stored here
        self.display_image_height = 600 if not display_image_height else display_image_height

        # Initialise Thread
        threading.Thread.__init__(self)
        self.daemon = True
        
        # Lock for setting crop radius, plus an exit flag
        self.lock = threading.Lock()
        self.exit_flag = False

        # Connect to camera
        self.camera = PointGreyCamera()
        self.camera.start()

        # Start the Tracker
        dims = dimensions.load()
        dims["num_cells"] = num_cells
        self.dims = dims

        self.tracker_settings = tracker_settings

        # Start the info window
        self.start()

    # TODO: THREAD SAFETY ON TRACKER
    def run(self, center_point_radius=20, color=[255,0,0], window_name="calibrator"):
        """
            Thread to display to the user
        """
        self.exit_flag = False

        while not self.exit_flag:
            # Get a frame
            frame = self.camera.get_frame()

            tracker = Tracker(frame, self.dims, **self.tracker_settings)

            # Make the masked version
            masked = tracker.crop(frame)
            
            contours = tracker.get_contour(frame)
            thresholded = cv2.cvtColor(tracker.threshold(frame), cv2.COLOR_GRAY2BGR)
        
            # Draw onto both images
            center = utils.image_center(frame)
            crop_center = tracker.center
            for img in [masked, frame, thresholded]:
                # Center Point
                cv2.circle(img, crop_center, center_point_radius, color, -1)

                # Datum
                cv2.arrowedLine(img, crop_center, (crop_center[0], crop_center[1] - tracker.crop_radius), color, thickness=5)

                # Contours
                cv2.drawContours(img, contours, -1, [0, 255, 0], thickness=3)

                # Cell Ordering
                for i, cell in enumerate(tracker.membrane.cells):
                    text=str(i+1)
                    font=cv2.FONT_HERSHEY_SIMPLEX
                    size=2.3
                    thickness=5
                    x, y = cv2.getTextSize(text, font, size, thickness)[0]
                    center = (int(cell.initial_centroid[0]) - x/2, int(cell.initial_centroid[1]) + y/2) 

                    cv2.putText(img, 
                        text, 
                        center, 
                        font, 
                        size, 
                        [200]*3, # Greyscale
                        thickness
                    )

            # Concatenate
            row1 = np.hstack([frame, masked])

            # Histogram
            fig = Figure()
            canvas = FigureCanvas(fig)
            ax = fig.gca()
            ax.hist(thresholded.ravel())

            # ThresholdLine
            ax.axvline(tracker.binary_threshold, linestyle="dashed", linewidth=3, color='k')                

            fig.suptitle('Greyscale Histogram', fontsize=20)
            ax.yaxis.set_visible(False)
            ax.tick_params(labelsize=15)

            # Render the histogram
            canvas.draw()

            # Retrieve the rendered frame
            width, height = fig.get_size_inches() * fig.get_dpi()
            histogram =  np.fromstring(canvas.tostring_rgb(), dtype='uint8').reshape(int(height), int(width), 3)
            histogram = cv2.resize(histogram, (thresholded.shape[:2]))

            # Make bottom row
            row2 = np.hstack([histogram, thresholded])

            # Show to user
            combined = np.vstack([row1, row2])
            combined = cv2.cvtColor(combined, cv2.COLOR_RGB2BGR)
            combined = utils.resize(combined, self.display_image_height)

            # Display to user
            cv2.imshow(window_name, combined)
            if cv2.waitKey(int(1000.0/self.camera.frame_rate)) == ord('q'):
                break

        # Cleanup
        cv2.destroyWindow(window_name)
        self.camera.stop()

    # Thread safe get/set
    # TODO: DRY?
    def set_crop_radius(self, crop_radius):
        with self.lock:
            self.tracker_settings["crop_radius"] = crop_radius

    def get_crop_radius(self):
        with self.lock:
            return self.tracker_settings["crop_radius"]

    def set_threshold(self, binary_threshold):
        with self.lock:
            self.tracker_settings["binary_threshold"] = binary_threshold

    def get_threshold(self):
        with self.lock:
            return self.tracker_settings["binary_threshold"]

    def set_x(self, x):
        with self.lock:
            self.tracker_settings["center"] = ([x, self.tracker_settings["center"][1]])

    def set_y(self, y):
        with self.lock:
            self.tracker_settings["center"] = ([self.tracker_settings["center"][0], y])

    def get_center(self):
        with self.lock:
            return self.tracker_settings["center"]

    # Exit the camera thread
    def quit(self):
        self.exit_flag = True
        self.join()

def main(args):
    # Construct the argument parse and parse the arguments
    argument_parse = argparse.ArgumentParser()

    argument_parse.add_argument("-n","--num-cells", 
        help="number of cells to track",
        type=int, 
        default=0
    )
    argument_parse.add_argument("-d", "--display-height", type=int, default=None, help="Display Image Height")
    args = argument_parse.parse_args(args)

    # Load settings
    default_settings = settings.load()

    calibrator = Calibrator(default_settings["tracker"], num_cells=args.num_cells, display_image_height=args.display_height)

    # Loop until the user signals
    show_settings = True
    while not calibrator.exit_flag:
        if show_settings:
            print "*"*30
            print "Current Settings:"
            print "    crop radius:", calibrator.get_crop_radius()
            print "    center:", calibrator.get_center()
            print "    threshold:", calibrator.get_threshold()
            print "Enter a command"
            print "          set crop radius | radius=<int>"
            print "     set binary threshold | threshold=<int>"
            print "             set x Center | x=<int>"
            print "             set y Center | y=<int>"
            print "         save to settings | save"
            print "                     quit | q"

        # Get input form the user
        user_input = raw_input("")
        radius_match = re.search(r"radius=(\d+)", user_input)
        threshold_match = re.search(r"threshold=(\d+)", user_input)
        x_match = re.search(r"x=(\d+)", user_input)
        y_match = re.search(r"y=(\d+)", user_input)

        # Default to show settings unless the option otherwise instructs
        show_settings = True

        # Choose option
        if radius_match:
            calibrator.set_crop_radius(int(radius_match.group(1)))

        elif threshold_match:
            calibrator.set_threshold(int(threshold_match.group(1)))

        elif x_match:
            calibrator.set_x((int(x_match.group(1))))

        elif y_match:
            calibrator.set_y((int(y_match.group(1))))
        
        elif user_input == "save":
            default_settings["tracker"]["crop_radius"] = calibrator.get_crop_radius()
            default_settings["tracker"]["center"] = calibrator.get_center()
            default_settings["tracker"]["binary_threshold"] = calibrator.get_threshold()
            
            settings.save(default_settings)
            print "SETTINGS SAVED"
            show_settings = False

        elif user_input=="q" or user_input=="quit":
            calibrator.quit()

        else:
            print "COMMAND NOT RECOGNISED"
            show_settings = False

if __name__ == "__main__":
    main(sys.argsv[1:])

