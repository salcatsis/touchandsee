import argparse
import scipy.misc

from . import render_video
from . import plot_image
from . import Tracker

from . import dimensions
from . import default_settings

import skvideo.io

# Construct the argument parse and parse the arguments
# TODO: Tidy up inpout args (use modes)
argument_parse = argparse.ArgumentParser()

argument_parse.add_argument('num_cells', type=int, help="number of cells to track")
argument_parse.add_argument('--infile', help="input video")
argument_parse.add_argument('--outfile', help="output video")
argument_parse.add_argument('--image', help="plot image")
argument_parse.add_argument('--plot', help="Show a plot", action="store_true")

args = argument_parse.parse_args()

dims = dimensions.load()
dims["num_cells"] = args.num_cells

if args.image:
    image = scipy.misc.imread(args.image)
    tracker = Tracker(image, dims)
    plot_image(tracker, image)

else:
    video = skvideo.io.vreader(args.infile)

    settings = default_settings.load()["tracker"]
    tracker = Tracker(video.next(), dims, **settings)

    render_video(tracker, args.infile, args.outfile, plot=args.plot)
