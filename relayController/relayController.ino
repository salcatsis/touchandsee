#include <Servo.h>

String inputString = "";
boolean stringComplete = false;

int nPins = 5;
int powerPins[] = {45,47,49,51,53};
int groundPins[] = {44,46,48,50,52};

int analogInPin = A0;
int voltageReadingRate = 50; //Hz
int nextVoltageRead = 0;

// Servo Parameters
Servo servo;
int servoPin = 12;
float servoDelay = 0.015;
int sweepAngle = 120;
float sweepTime = 4;


// Internal Servo Parameters
bool sweeping = false;
int sweepStartTime = 0;
int sweepNextTime = 0;

// Hold Params
bool holding = false;
int holdAngle = 0;
int holdAngularVelocity = 20;
int holdStartTime = 0;
int holdNextTime = 0;
int holdTime = 0;

float pi = 3.14159265359;

void setup() {

  // Initialise Relays
  for (int i=0; i < nPins; i++){
    pinMode(powerPins[i],OUTPUT);
    digitalWrite(powerPins[i],LOW);
    
    pinMode(groundPins[i],OUTPUT);
    digitalWrite(groundPins[i],HIGH);
  }

  // Initialise Servo
  servo.attach(servoPin);
  servo.write(0);
  
  Serial.begin(115200);
}

void loop() {
  // Servo Sweep
  if(sweeping)
  {  
    sweep();
  }
  else if(holding)
  {
    hold();
  }

  // Serial Command Handling
  if (!stringComplete)
  {
    return;  
  }

  stringComplete = false;
   
  if(inputString[0] == 's')
  {
      initSweep();
  }
  else if(inputString[0] == 'h')
  {
      initHold();
  }
  else 
  {
      setPins();
  }

  // Relay to user
  inputString = "";
  Serial.println("1");   
}

void initHold()
{
  int colonIndex = inputString.indexOf(':');
  holdAngle = inputString.substring(1, colonIndex).toInt();
  holdTime = inputString.substring(colonIndex+1).toFloat();
  
  // Determine end times and next sweep
  float t = float(millis())/1000;
  holdStartTime = t;
  servo.write(0);
  holdNextTime = servoDelay;
  holding = true;
}

void hold()
{
  float t = float(millis())/1000 - holdStartTime;

  // Check if finished
  if(t > holdTime)
  {
    holding = false;
    servo.write(0);
    return;
  }  

  if(t > holdNextTime)
  {
    // Determine Angle
    int angle = t*holdAngularVelocity;
    if(angle > holdAngle){
      angle = holdAngle;
    }

    // Update Servo
    servo.write(angle);
    
    // Indicate next update
    holdNextTime += servoDelay;
  }


  
}

void initSweep()
{
  int colonIndex = inputString.indexOf(':');
  sweepAngle = inputString.substring(1, colonIndex).toInt();
  sweepTime = inputString.substring(colonIndex+1).toFloat();

  // Get the sweep Angle
  sweepAngle = inputString.substring(1).toInt();
  
  // Determine end times and next sweep
  float t = float(millis())/1000;
  sweepStartTime = t;
  
  sweepNextTime = servoDelay;
  sweeping = true;
}
  
void sweep()
{
  float t = float(millis())/1000 - sweepStartTime;

  // End time
  if(t >= sweepTime)
  {
    // Reset
    servo.write(0);
    sweeping = false;
    return;
  }

  // Update Servo
  if(t >= sweepNextTime){
    float f = 1/float(sweepTime);
    
    int pos = int(sweepAngle*0.5*(1 - cos(2*pi*f*t)));
    servo.write(pos);

    sweepNextTime += servoDelay;
  }
}


void setPins() 
{
  int loopLength = inputString.length() < nPins ? inputString.length() : nPins;

  for (int i=0; i < loopLength; i++)
  {
        
    if (inputString[i]== '0') 
    {
      digitalWrite(groundPins[i],HIGH);
      digitalWrite(powerPins[i],LOW);          
    }
    else if (inputString[i] == '1') 
    {
      digitalWrite(groundPins[i],LOW);
      digitalWrite(powerPins[i],HIGH);
    }
  }
}


// thanks https://www.arduino.cc/en/Tutorial/SerialEvent
void serialEvent() {
  while (Serial.available() && !stringComplete) {
    // get the new byte:
    char inChar = (char)Serial.read();
    
    // if the incoming character is a newline, set a flag
    // so the main loop can do something about it:
    if (inChar == '\n') {
      stringComplete = true;
      break;
    }

    inputString += inChar;
  }
}
